package asteroids.model;

import be.kuleuven.cs.som.annotate.*;
import asteroids.util.*;

/**
 * A class for dealing with spaceships, containing all methods affecting their parameters,
 * such as position, velocity, orientation, radius, mass, speed limit and minimal radius.<br>
 * It also deals with possible actions specific for the spaceship, like turning, thrusting or firing a
 * bullet. It inherits the methods written in SpaceObject as well.
 * 
 * @invar	The velocity of each spaceship must be a valid velocity for a spaceship.<br>
 * 	<code>	| isValidVelocity(getVelocity()) </code>
 * @invar 	The direction of each spaceship must be a valid direction for a spaceship.<br>
 * 	<code>	| isValidDirection(getDirection())</code>
 * @invar	The radius of each spaceship must be a valid radius for a spaceship.<br>
 * 	<code>	| isValidRadius(getRadius())</code>
 * @invar	The mass of each spaceship must be a valid mass for a spaceship.<br>
 * 	<code>	| isValidMass(getMass())</code>
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.4.3 (pre 3.0)
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-2
 */

public class Ship extends SpaceObject {		//implements IShip omitted
	
	/**
	 * Initialise the new spaceship with a given horizontal and vertical position,
	 * horizontal and vertical velocity, direction, radius, speed limit and mass.
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the new spaceship.
	 * @param 	yPosition
	 * 			The vertical position of the new spaceship.
	 * @param 	xVelocity
	 * 			The horizontal velocity of the new spaceship.
	 * @param	yVelocity
	 * 			The vertical velocity of the new spaceship.
	 * @param 	direction
	 * 			The direction of the new spaceship.
	 * @param 	radius
	 * 			The radius of the new spaceship.
	 * @param	mass
	 * 			The mass of the new spaceship. 
	 * @pre		The given direction must be a valid direction for the spaceship.<br>
	 * 	<code> 	| isValidDirection(direction) </code><br>
	 * @post	The horizontal position of the new spaceship is equal to the given horizontal position.<br>
	 * 	<code> 	| (new this).getXPosition() == xPosition</code><br>
	 * @post	The vertical position of the new spaceship is equal to the given vertical position.<br>
	 * 	<code> 	| (new this).getYPosition() == yPosition</code><br>
	 * @post	The horizontal velocity of the new spaceship is equal to the given horizontal velocity.<br>
	 * 	<code> 	| (new this).getXVelocity() == xVelocity</code><br>
	 * @post	The vertical velocity of the new spaceship is equal to the given vertical velocity.<br>
	 * 	<code> 	| (new this).getYVelocity() == yVelocity</code><br>
	 * @post	The direction of the new spaceship is equal to the given direction.<br>
	 * 	<code> 	| (new this).getDirection() == direction</code><br>
	 * @post    The radius of the new spaceship is equal to the given radius.<br>
	 * 	<code> 	| (new this).getRadius() == radius</code><br>
	 * @post	The mass of the new spaceship is equal to the given mass.<br>
	 * 	<code>	| (new this).getMass() == mass</code><br>
	 * @post	The speed limit of the new spaceship is equal to the speed of light.<br>
	 * 	<code>	| (new this).getSpeedLimit() == LIGHT_SPEED</code><br>
	 * @post	The minimal radius of the new spaceship is equal to 0.0 kilometers.<br>
	 * 	<code>	| (new this).getMinRadius() == 0.0</code><br>
	 * @post	The thruster of the new spaceship is equal to 1.1E21 Newton.<br>
	 *  <code>	| (new this).getThruster() == 1.1E21 </code><br>
	 * @throws 	IllegalArgumentException
	 * 			The given radius is not a valid radius for the given spaceship.<br>
	 * 	<code> 	| !isValidRadius(radius)</code><br>
	 * @throws	IllegalArgumentException
	 * 			The given mass is not a valid mass for the given spaceship.<br>
	 * 	<code>	| !isValidMass(mass)</code><br>
	 */
	public Ship(double xPosition, double yPosition, double xVelocity, double yVelocity, 
					double direction, double radius, double mass) 
					throws IllegalArgumentException {
			super(xPosition, yPosition, xVelocity, yVelocity, radius, mass, LIGHT_SPEED, 0.0);
			setDirection(direction);
		thruster = 1.1E21;
	}
	
	/**
	 * Initialise a default spaceship with a default horizontal and vertical position,
	 * horizontal and vertical velocity, direction, radius, mass, speed limit and minimal radius.
	 * 
	 * @post	The horizontal position of the new spaceship is equal to the default horizontal position.<br>
	 * 	<code> 	| (new this).getXPosition() == 100.0</code>
	 * @post	The vertical position of the new spaceship is equal to the default vertical position.<br>
	 * 	<code> 	| (new this).getYPosition() == 100.0</code>
	 * @post	The horizontal velocity of the new spaceship is equal to the default horizontal velocity.<br>
	 * 	<code> 	| (new this).getXVelocity() == 10.0</code>
	 * @post	The vertical velocity of the new spaceship is equal to the default vertical velocity.<br>
	 * 	<code> 	| (new this).getYVelocity() == 20.0</code>
	 * @post	The direction of the new spaceship is equal to the default direction.<br>
	 * 	<code> 	| (new this).getDirection() == Math.PI/5</code>
	 * @post    The radius of the new spaceship is equal to the default radius.<br>
	 * 	<code> 	| (new this).getRadius() == 16.0</code>
	 * @post	The mass of the new spaceship is equal to the default mass.<br>
	 * 	<code>	| (new this).getMass() == 5E15</code><br>
	 * @post	The speed limit of the new spaceship is equal to the speed of light.<br>
	 * 	<code>	| (new this).getSpeedLimit() == LIGHT_SPEED</code><br>
	 * @post	The minimal radius of the new spaceship is equal to 0.0 kilometers.<br>
	 * 	<code>	| (new this).getMinRadius() == 0.0</code><br>
	 * @post	The thruster of the new spaceship is equal to 1.1E21 Newton.<br>
	 *  <code>	| (new this).getThruster() == 1.1E21 </code><br>
	 * @throws 	IllegalArgumentException
	 * 			The given radius is not a valid radius for the given spaceship.<br>
	 * 	<code> 	| !isValidRadius(radius)</code>
	 * @throws	IllegalArgumentException
	 * 			The given mass is not a valid mass for the given spaceship.<br>
	 * 	<code>	| !isValidMass(mass)</code>
	 */
	public Ship() throws IllegalArgumentException, IllegalStateException {
		this(100.0,100.0,10.0,20.0,Math.PI/5,16.0,5E15);
	}
	
	/**
	 * Return the direction of the spaceship, expressed in radians.<br>
	 * NOMINAL
	 * 	
	 * @return 	The direction of the spaceship
	 */
	@Basic @Raw
	public double getDirection(){
		return this.direction;
	}
	
	/**
	 * Sets the direction of the spaceship to the given entry.
	 * 
	 * @param 	direction
	 * 			The new direction of the spaceship
	 * @pre		The direction of the spaceship must be a valid direction.<br>
	 * 	<code>	| isValidDirection(direction)</code><br>
	 * @post	The direction of the spaceship is equal to the given direction.<br>
	 * 	<code>	| (new this).getDirection() == direction</code><br>
	 */
	@Model @Raw
	private void setDirection(double direction){	
		assert isValidDirection(direction);				
		this.direction = direction;
	}
	
	/**
	 * Checks if the given direction is a valid direction for the spaceship. 
	 * 
	 * @param 	direction
	 * 			The direction that has to be checked.
	 * @pre		The given direction must be a valid number (and nothing else).<br>
	 * 	<code>	| !Double.isNaN(direction) </code><br>
	 */
	public static boolean isValidDirection(double direction){	
		return !Double.isNaN(direction); 						
	}
	
	/**
	 * Variable storing the direction of the spaceship.
	 */
	private double direction;
	
	/**
	 * Checks whether the space object is a ship.
	 * 
	 * @return	True if and only if the space object is a ship.
	 * @note	This method overrides the same method in the class SpaceObject.
	 */
	@Override
	public boolean isShip() {
		return true;
	}

	/**
	 * Rotates the direction of the spaceship over a given angle.
	 * NOMINAL
	 * 
	 * @param 	angle
	 * 			The angle over which the spaceship rotates.
	 * @pre		The new direction of the spaceship must be a valid direction.<br>
	 * 	<code>	| isValidDirection(getDirection() + angle)</code><br>
	 * @post	The new direction of the spaceship is equal to its actual direction increased
	 * 			by the given angle.<br>
	 *	<code>	| (new this).getDirection() == (this.getDirection() + angle) </code><br>
	 * @note	@Raw because the new angle may violate the class invariant during the method.
	 */
	@Raw
	public void turn(double angle) {
		assert isValidDirection(getDirection() + angle);
		this.setDirection(getDirection() + angle);
	}
	
	/**
	 * Thrusts the spaceship for a given duration, according to the law of motion.
	 * 
	 * @param 	duration
	 * 			The duration of the thrust.
	 * @post	If the thruster is on, 
	 * 			the velocity is increased for a given duration after being adapted.
	 * 			The acceleration is proportional to its mass and the given duration.<br>
	 * <code><p>| if(this.checkThrusterIsOn())<br>
	 * 			|	then let acceleration = this.getThruster()*duration/(this.getMass()*1000)</p>
	 * 		<p>	|		 in<br>
	 * 			| 			(new this).getXVelocity() == this.getXVelocity() + acceleration*Math.cos(this.getDirection())<br>
	 * 			| 			(new this).getYVelocity() == this.getYVelocity() + acceleration*Math.sin(this.getDirection())<br>
	 * @post	The velocity does not change if the given duration is not a positive number.</p></code>
	 * 	<code>	| if((Double.isNaN(duration) || (duration < 0))) <br>
	 * 			| 	then ((new this).getXVelocity() == this.getXVelocity() <br>
	 * 			|	&&	  (new this).getYVelocity() == this.getYVelocity()) </code>
	 */
	@Model
	private void thrust(double duration){
		if(!isValidDuration(duration))
			duration = 0.0;
		if(this.checkThrusterIsOn()){
			//*1000 in the denominator because we have to convert m/s� into km/s�
			double acceleration = this.getThruster()*duration/(this.getMass()*1000); 
			Vector thrust = Vector.polarVector(acceleration, this.getDirection());
			this.setVelocity(thrust.X_COORD + this.getXVelocity(), thrust.Y_COORD + this.getYVelocity());
		}
	}
	
	/**
	 * Returns the value of the thruster of the spaceship.
	 * 
	 * @return	The value of the thruster of the spaceship.
	 */
	@Basic @Immutable
	public double getThruster() {
		return this.thruster;
	}
	
	/**
	 * Constant storing the power of the spaceship's thruster.
	 */
	private final double thruster;
	
	/**
	 * Checks whether the spaceship's thruster is enabled.
	 * 
	 * @return	True if the spaceship's thruster is on.
	 */
	@Basic
	public boolean checkThrusterIsOn(){
		return this.thrusterIsOn;
	}
	
	/**
	 * Sets the state of the thruster on or off.
	 * 
	 * @param 	active
	 * 			The state in which the thruster has to be set.
	 */
	public void setThrusterOn(boolean active) {
		this.thrusterIsOn = active;
	}
	
	/**
	 * Variable storing whether the spaceship's thruster is enabled.
	 */
	private boolean thrusterIsOn;
	
	/**
	 * Returns the position vector of the fired bullet.
	 * 
	 * @return	The horizontal position of the fired bullet is equal to the actual horizontal 
	 * 			position of the spaceship augmented by the ship's radius plus 3.0 projected 
	 * 			on the horizontal axis.
	 * <code>	| result.getXPosition() == this.getXPosition() + (this.getRadius() + 3.0)*Math.cos(this.getDirection())</code><br>
	 * @return	The vertical position of the fired bullet is equal to the actual vertical 
	 * 			position of the spaceship augmented by the ship's radius plus 3.0 projected 
	 * 			on the vertical axis.
	 * <code>	| result.getYPosition() == this.getYPosition() + (this.getRadius() + 3.0)*Math.sin(this.getDirection())</code><br>
	 */
	private Vector getFiredBulletPosition() {
		return Vector.addition(this.getPosition(), Vector.polarVector(this.getRadius() + 3.0, this.getDirection()));
	}
	
	/**
	 * Returns the initial velocity vector of the fired bullet.
	 * 	
	 * @return	The horizontal velocity of the fired bullet is equal to its default initial speed
	 * 			projected on the horizontal axis.
	 * 	<code>	| result.getXVelocity() == Bullet.INITIAL_SPEED*Math.cos(this.getDirection())</code><br>
	 * @return	The vertical velocity of the fired bullet is equal to its default initial speed
	 * 			projected on the vertical axis.
	 * 	<code>	| result.getYVelocity() == Bullet.INITIAL_SPEED*Math.sin(this.getDirection())</code><br>
	 */
	public Vector getInitialBulletVelocity() {
		return Vector.polarVector(Bullet.INITIAL_SPEED, this.getDirection());
	}
	
	/**
	 * Makes the ship fire a bullet at the same place as the canon position.
	 * 
	 * @result	If the spaceship has a world attached to it,
	 * 			creates a new bullet at the same position as the canon, with the default initial velocity
	 * 			with the default bullet radius and with the spaceship as a source.<br> 
	 * 	<code>	| if (!this.hasAsWorld(null))<br>
	 * 			|	then (result.getXPosition() == this.getCanonPosition().X_COORD<br>
	 * 			|		&& result.getYPosition() == this.getCanonPosition().Y_COORD<br>
	 * 			|		&& result.getXVelocity() == this.getInitialBulletVelocity().X_COORD<br>
	 * 			|		&& result.getYVelocity() == this.getInitialBulletVelocity().Y_COORD<br>
	 * 			|		&& result.getRadius() == 3.0<br>
	 * 			|		&& result.getSource() == this)<br></code>
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the spaceship doesn't have a world attached to it.<br>
	 * 	<code>	| (this.hasAsWorld(null)) </code><br>
	 * @throws 	IllegalArgumentException
	 * 			Will throw an exception if one of the constructor of Bullet returns IllegalArgumentException.
	 */
	public Bullet fireBullet() throws IllegalStateException, IllegalArgumentException {
		if (this.hasAsWorld(null)){
			throw new IllegalStateException();
		}	
		else {
			return new Bullet(this.getFiredBulletPosition().X_COORD, this.getFiredBulletPosition().Y_COORD, 
				this.getInitialBulletVelocity().X_COORD, this.getInitialBulletVelocity().Y_COORD, 3.0, this);
		}
	}
	
	/**
	 * Makes this ship collide with another space object.
	 * 
	 * @param	spaceObject
	 * 			The other space object
	 * @effect	If the other space object is a ship,
	 * 			both ships will bounce off each other and they will remember they collided recently.<br>
	 * 	<code>	| if (spaceObject.isShip())<br>
	 * 			|	then (this.bounce(spaceObject)<br>
	 * 			|		&& this.setRecentCollision(true)<br>
	 * 			|		&& spaceObject.setRecentCollision(true))</code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this spaceship or the other space object 
	 * 			doesn't have a world attached to it.<br>
	 * 	<code>	| (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))</code>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this spaceship and the other space object
	 * 			don't navigate inside the same world.<br>
	 * 	<code>	| (!this.hasAsWorld(spaceObject.getWorld()))</code>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the distance between this spaceship
	 * 			and the other space object is not equal to 0.0.<br>
	 * 	<code>	| (!Util.fuzzyEquals(this.getDistanceBetween(spaceObject),0.0))</code>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the given space object is a bullet, because the class Bullet
	 * 			already cares about collisions between a ship and a bullet.<br>
	 * 	<code>	(spaceObject.isBullet())</code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the given space object is an asteroid, because the class Asteroid
	 * 			already cares about collisions between a ship and an asteroid.<br>
	 * 	<code>	(spaceObject.isAsteroid())</code><br>
	 * @throws	NullPointerException
	 * 			Will throw an exception if this spaceship or the other space object is <code>null</code>.<br>
	 * 	<code>	| (this == null || spaceObject == null) </code>
	 * @note 	Testing on the terminated state of the objects will probably not be necessary anymore,
	 * 			because the method hasAsWorld() already cares about that.
	 */
	@Override
	public void collide(SpaceObject spaceObject) throws IllegalStateException, NullPointerException {
		if (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
			throw new IllegalStateException();
		if (!this.hasAsWorld(spaceObject.getWorld()))
			throw new IllegalStateException();
		if (!Util.fuzzyEquals(this.getDistanceBetween(spaceObject),0.0))
			throw new IllegalStateException();
		if (spaceObject.isBullet())
			throw new IllegalStateException();
		if (spaceObject.isAsteroid())
			throw new IllegalStateException();
		if (spaceObject.isShip()) {
			this.bounce(spaceObject);
			this.setRecentCollision(true);
			spaceObject.setRecentCollision(true);
		}
	}
	
	/**
	 * Makes the spaceship evolve together with its world for a given duration.
	 * 
	 * @param 	duration
	 * 			The given duration of the evolution.
	 * @effect	The spaceship will move for a given duration.<br>
	 * 	<code>	| this.move(duration) </code><br>
	 * @effect	The spaceship will thrust for a given duration.<br>
	 * 	<code>	| this.thrust(duration) </code><br>
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the spaceship doesn't have a world attached to it.<br>
	 *  <code>	| (this.hasAsWorld(null)) </code><br>
	 */
	@Override
	public void evolve(double duration) throws IllegalStateException {
		super.evolve(duration);
		this.thrust(duration);
	}

}
