package asteroids.model;

import java.util.*;

import asteroids.control.*;
import asteroids.exception.ModelException;
import asteroids.view.CollisionListener;

/**
 * A class for reducing dependencies of outside code on the inner workings of the Ship class and
 * for making it easier to use.
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version	2.3
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-1
 */
public class Facade implements IFacade<World, Ship, Asteroid, Bullet> {
	
//	@Override
//	public World createWorld() throws ModelException {
//		try {
//			return new World(2000,2000);
//		} catch (IllegalArgumentException exc) {
//			throw new ModelException(exc);
//		}
//	}

	
	@Override
	public World createWorld(double width, double height) throws ModelException {
		try {
			return new World(height,width);
		} catch (IllegalArgumentException exc) {
			throw new ModelException("The world could not be created because one or both dimensions were not valid.");
		}
	}
	
	@Override
	public double getWorldWidth(World world) {
		return world.getWidth();
	}
	
	@Override
	public double getWorldHeight(World world) {
		return world.getHeight();
	}
	
	@Override
	public Set<Ship> getShips(World world) {
		return world.getShips();
	}
	
	@Override
	public Set<Asteroid> getAsteroids(World world) {
		return world.getAsteroids();
	}

	@Override
	public Set<Bullet> getBullets(World world) {
		return world.getBullets();
	}
	
	@Override
	public void addShip(World world, Ship ship) {
		try {
			world.addShip(ship);
		} catch (IllegalArgumentException exc) {
			throw new ModelException("The world cannot have the given ship as a space object.");
		}
	}
	
	@Override
	public void removeShip(World world, Ship ship) {
		try {
			world.removeShip(ship);
		} catch (Exception exc) {
			throw new ModelException("The world doesn't have the given ship as a space object.");
		}
	}
	
	@Override
	public void addAsteroid(World world, Asteroid asteroid) {
		try {
			world.addAsteroid(asteroid);
		} catch (IllegalArgumentException exc) {
			throw new ModelException("The world cannot have the given asteroid as a space object.");
		}
	}
	
	@Override
	public void removeAsteroid(World world, Asteroid asteroid) {
		try {
			world.removeAsteroid(asteroid);
		} catch (Exception exc) {
			throw new ModelException("The world doesn't have the given asteroid as a space object.");
		}
	}
	
	@Override
	public void evolve(World world, double dt, CollisionListener collisionListener) {
		try {
			world.evolve(dt, collisionListener);
		} catch (Exception exc) {
			throw new ModelException("The world could not evolve because it couldn't simulate any collisions.");
		}
	}
	
//	@Override
//	public Ship createShip() throws ModelException {
//		try {
//			return createShip(0, 0, 0, 0, 10+Double.MIN_VALUE , 0,100000,4500);  
//		  } catch (IllegalArgumentException exc) {
//			  throw new ModelException(exc);
//		  }
//	}
	
	@Override
	public Ship createShip(double x, double y, double xVelocity,
			double yVelocity, double radius, double angle, double mass) throws ModelException {
		try {
			  return new Ship(x, y, xVelocity, yVelocity, angle, radius, mass);
		  } catch (IllegalArgumentException exc) {
			  throw new ModelException("The given mass and/or the given radius are not valid for this ship.");
		  }
	}
	
	@Override
	public boolean isShip(Object o) throws ModelException {
		if (!SpaceObject.class.isInstance(o))
			throw new ModelException("This is not a space object.");
		return ((SpaceObject) o).isShip();
	}

	@Override
	public double getShipX(Ship ship) {
		return ship.getXPosition();
	}

	@Override
	public double getShipY(Ship ship) {
		return ship.getYPosition();
	}

	@Override
	public double getShipXVelocity(Ship ship) {
		return ship.getXVelocity();
	}

	@Override
	public double getShipYVelocity(Ship ship) {
		return ship.getYVelocity();
	}

	@Override
	public double getShipRadius(Ship ship) {
		return ship.getRadius();
	}

	@Override
	public double getShipDirection(Ship ship) {
		return ship.getDirection();
	}
	
	@Override
	public double getShipMass(Ship ship) {
		return ship.getMass();
	}
	
	@Override
	public World getShipWorld(Ship ship) {	//wat doen als world == null? exception gooien?
		return ship.getWorld();
	}

	@Override
	public boolean isShipThrusterActive(Ship ship) {
		return ship.checkThrusterIsOn();
	}
	
	@Override
	public void setThrusterActive(Ship ship, boolean active) {
		ship.setThrusterOn(active);
	}
	
	@Override
	public void fireBullet(Ship ship) {
		try {
			ship.fireBullet();
		} catch (Exception exc) {
			throw new ModelException("This ship doesn't belong to a world or the fired bullet could not fit within the ship's world.");
		}
	}
	
	
//	@Override
//	public void move(Ship ship, double dt) throws ModelException {
//		try {
//			  ship.move(dt);
//		  } catch (IllegalArgumentException exc) {
//			  throw new ModelException(exc);
//		  }	
//	}
//
//	@Override
//	public void thrust(Ship ship, double amount) {
//		ship.thrust(amount);
//	}

	@Override
	public void turn(Ship ship, double angle) {
		ship.turn(angle);
	}

//	@Override
//	public double getDistanceBetween(Ship ship1, Ship ship2) throws ModelException {
//		try {
//			  return ship1.getDistanceBetween(ship2); 
//		  } catch (IllegalArgumentException exc) {
//			  throw new ModelException(exc);
//		  }
//	}
//
//	@Override
//	public boolean overlap(Ship ship1, IShip ship2) throws ModelException {
//		try {
//			  return ship1.overlap(ship2);
//		  } catch (IllegalArgumentException exc) {
//			  throw new ModelException(exc);
//		  }	
//	}
//
//	@Override
//	public double getTimeToCollision(Ship ship1, Ship ship2) throws ModelException {
//		try {
//			  return ship1.getTimeToCollision(ship2);
//		  } catch (IllegalArgumentException exc) {
//			  throw new ModelException(exc);
//		  }
//	}
//
//	@Override
//	public double[] getCollisionPosition(IShip ship1, IShip ship2) throws ModelException {
//		try {
//			  return ship1.getCollisionPosition(ship2);
//		  } catch (IllegalArgumentException exc) {
//			  throw new ModelException(exc);
//		  }
//	}
	
	@Override
	public Asteroid createAsteroid(double x, double y, double xVelocity, double yVelocity, 
			double radius) throws ModelException {
		try {
			return new Asteroid(x, y, xVelocity, yVelocity, radius);
		} catch (IllegalArgumentException exc) {
			throw new ModelException("The given mass and/or the given radius are not valid for this asteroid.");
		}
	}
	
	@Override
	public Asteroid createAsteroid(double x, double y, double xVelocity, double yVelocity, 
			double radius, Random random) throws ModelException {
		try {
			return new Asteroid(x, y, xVelocity, yVelocity, radius, random);
		} catch (IllegalArgumentException exc) {
			throw new ModelException("The given mass and/or the given radius are not valid for this asteroid.");
		}
	}
	
	@Override
	public boolean isAsteroid(Object o) throws ModelException {
		if (!SpaceObject.class.isInstance(o))
			throw new ModelException("This is not a space object.");
		return ((SpaceObject) o).isAsteroid();
	}
	
	@Override
	public double getAsteroidX(Asteroid asteroid) {
		return asteroid.getXPosition();
	}

	@Override
	public double getAsteroidY(Asteroid asteroid) {
		return asteroid.getYPosition();
	}
	
	@Override
	public double getAsteroidXVelocity(Asteroid asteroid) {
		return asteroid.getXVelocity();
	}
	
	@Override
	public double getAsteroidYVelocity(Asteroid asteroid) {
		return asteroid.getYVelocity();
	}
	
	@Override
	public double getAsteroidRadius(Asteroid asteroid) {
		return asteroid.getRadius();
	}
	
	@Override
	public double getAsteroidMass(Asteroid asteroid) {
		return asteroid.getMass();
	}
	
	@Override
	public World getAsteroidWorld(Asteroid asteroid) {
		return asteroid.getWorld();
	}
	
	@Override
	public boolean isBullets(Object o) throws ModelException {
		if (!SpaceObject.class.isInstance(o))
			throw new ModelException("This is not a space object.");
		return ((SpaceObject) o).isBullet();
	}
	
	@Override
	public double getBulletX(Bullet bullet) {
		return bullet.getXPosition();
	}
	
	@Override
	public double getBulletY(Bullet bullet) {
		return bullet.getYPosition();
	}
	
	@Override
	public double getBulletXVelocity(Bullet bullet) {
		return bullet.getXVelocity();
	}
	
	@Override
	public double getBulletYVelocity(Bullet bullet) {
		return bullet.getYVelocity();
	}
	
	@Override
	public double getBulletRadius(Bullet bullet) {
		return bullet.getRadius();
	}
	
	@Override
	public double getBulletMass(Bullet bullet) {
		return bullet.getMass();
	}
	
	@Override
	public World getBulletWorld(Bullet bullet) {
		return bullet.getWorld();
	}
	
	@Override
	public Ship getBulletSource(Bullet bullet) {
		return bullet.getSource();
	}
	
}