package asteroids.model;

import asteroids.util.*;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class for dealing with projectiles, containing all methods affecting their parameters,
 * such as position, velocity, radius, speed limit, mass and source. It also deals with possible 
 * actions for the projectiles, like colliding and exploding in a specific way.
 * It inherits the methods written in SpaceObject as well.
 * 
 * @invar	The velocity of each projectile must be a valid velocity for a projectile.
 * 			| isValidVelocity(getVelocity())
 * @invar	The radius of each projectile must be a valid radius for a projectile.
 * 			| isValidRadius(getRadius())
 * @invar	The initial velocity of each projectile must be the valid velocity for a projectile.
 * 			| isValidInitialSpeed()
 * @invar	The mass of each projectile must be a valid mass for a projectile.
 * 			| isValidMass(getMass())
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.4.3 (pre 3.0)
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-2
 */

public class Bullet extends SpaceObject {
	
	/**
	 * Initialise the new projectile with a given horizontal and vertical position,
	 * horizontal and vertical velocity, a radius, a speed limit, a mass, a source and a bouncing state.
	 * 
	 * @param 	xPosition
	 * 			The horizontal position of the new projectile.
	 * @param 	yPosition
	 * 			The vertical position of the new projectile.
	 * @param 	xVelocity
	 * 			The horizontal velocity of the new projectile.
	 * @param	yVelocity
	 * 			The vertical velocity of the new projectile.
	 * @param 	radius
	 * 			The radius of the new projectile.
	 * @param	source
	 * 			The source of the new projectile.
	 * @post	The horizontal position of the new projectile is equal to the given horizontal position.
	 * 		  	| (new this).getXPosition() == xPosition
	 * @post	The vertical position of the new projectile is equal to the given vertical position.
	 * 		  	| (new this).getYPosition() == yPosition
	 * @post	The horizontal velocity of the new projectile is equal to the given horizontal velocity.
	 * 		  	| (new this).getXVelocity() == xVelocity
	 * @post	The vertical velocity of the new projectile is equal to the given vertical velocity.
	 * 		  	| (new this).getYVelocity() == yVelocity
	 * @post    The radius of the new projectile is equal to the given radius.
	 * 		  	| (new this).getRadius() == radius
  	 * @post	The minimal radius of the new projectile is equal to kilometers.
	 * 			| (new this).getMinRadius() == 0.0
	 * @post	The speed limit of the new projectile is equal to the speed of light.
	 * 			| (new this).getSpeedLimit() == LIGHT_SPEED
	 * @post	The mass of the new projectile is equal to the calculated mass.
	 * 			| (new this).getMass() == (4/3)*Math.PI*Math.pow(RADIUS, 3)*DENSITY
	 * @post 	The source of the new projectile is equal to the given source.
	 * 			| (new this).getSource() == source
	 * @throws 	IllegalArgumentException
	 * 			The given radius is not a valid radius for the given projectile.
	 * 		  	| !isValidRadius(radius)
	 * @throws	IllegalArgumentException
	 *			The calculated mass is not a valid mass for the given projectile.
	 *			| !isValidMass((4/3)*Math.PI*Math.pow(RADIUS, 3)*DENSITY) 
	 * @throws	IllegalArgumentException
	 * 			The initial speed is not a valid speed for the projectile.
	 * 			| !isValidInitialSpeed()
	 * @throws	NullPointerException
	 * 			The given source is null.
	 * 			| (source == null)
	 */
	public Bullet(double xPosition, double yPosition, double xVelocity, double yVelocity, 
			double radius, Ship source) throws IllegalArgumentException, NullPointerException {
			super(xPosition, yPosition, xVelocity, yVelocity, radius, (4/3)*Math.PI*Math.pow(radius, 3)*DENSITY, LIGHT_SPEED, 0.0);
		if (source == null)
			throw new IllegalArgumentException("The projectile doesn't match any source.");
		this.source = source;
		if (!isValidInitialSpeed())
			throw new IllegalArgumentException("The initial speed is not valid.");
		try {
			this.setWorld(source.getWorld());		
		} catch (IllegalArgumentException exc) {
			this.terminate();
		}
	}
	
	/**
	 * Initialise the new projectile with a default horizontal and vertical position,
	 * horizontal and vertical velocity, a radius, a speed limit, a mass, a source and a bouncing state.
	 * 
	 * @param	source
	 * 			The source of the new projectile.
	 * @post	The horizontal position of the new projectile is equal to the default horizontal position.
	 * 		  	| (new this).getXPosition() == source.getWorld().getHeight()/2
	 * @post	The vertical position of the new projectile is equal to the default vertical position.
	 * 		  	| (new this).getYPosition() == source.getWorld().getWidth()/2
	 * @post	The horizontal velocity of the new projectile is equal to the default horizontal velocity.
	 * 		  	| (new this).getXVelocity() == INITIAL_SPEED
	 * @post	The vertical velocity of the new projectile is equal to the default vertical velocity.
	 * 		  	| (new this).getYVelocity() == 0.0
	 * @post    The radius of the new projectile is equal to the default radius.
	 * 		  	| (new this).getRadius() == 3.0
  	 * @post	The minimal radius of the new projectile is equal to 0.0 kilometers.
	 * 			| (new this).getMinRadius() == 0.0
	 * @post	The speed limit of the new projectile is equal to the speed of light.
	 * 			| (new this).getSpeedLimit() == LIGHT_SPEED
	 * @post	The mass of the new projectile is equal to the calculated mass.
	 * 			| (new this).getMass() == (4/3)*Math.PI*Math.pow(RADIUS, 3)*DENSITY
	 * @post 	The source of the new projectile is equal to the given source.
	 * 			| (new this).getSource() == source
	 * @throws 	IllegalArgumentException
	 * 			The given radius is not a valid radius for the given projectile.
	 * 		  	| !isValidRadius(radius)
	 * @throws	IllegalArgumentException
	 *			The calculated mass is not a valid mass for the given projectile.
	 *			| !isValidMass((4/3)*Math.PI*Math.pow(RADIUS, 3)*DENSITY) 
	 * @throws	IllegalArgumentException
	 * 			The initial speed is not a valid speed for the projectile.
	 * 			| !isValidInitialSpeed()
	 * @throws	NullPointerException
	 * 			The given source is null.
	 * 			| (source == null)
	 */
	public Bullet(Ship source) throws IllegalArgumentException, NullPointerException {
		this(source.getWorld().getHeight()/2,source.getWorld().getWidth()/2, INITIAL_SPEED, 0.0, 3.0, source);
	}
	
	/**
	 * Constant storing the density of a projectile.
	 */
	public static final double DENSITY = 7.8E12;
	
	/**
	 * Returns the source of the fired projectile.
	 * 
	 * @return The source of the projectile.
	 */
	@Basic @Immutable
	public Ship getSource() {
		return this.source;
	}
	
	/**
	 * Constant storing the source of a projectile.
	 */
	private final Ship source;
	
	/**
	 * Checks if the initial speed of a projectile is a valid initial speed.
	 * 
	 * @return	True if and only if the initial speed of a projectile is equal to the default initial speed.		//is dit de goede manier van schrijven?
	 * 			| result == Util.fuzzyEquals(Vector.norm(this.getVelocity()),INITIAL_SPEED)
	 */
	private boolean isValidInitialSpeed() {
		return (Util.fuzzyEquals(Vector.norm(this.getVelocity()),INITIAL_SPEED));
	}
	
	/**
	 * Constant storing the initial speed of a bullet.
	 */
	public static final double INITIAL_SPEED = 250.0;
	
	/**
	 * Checks whether the space object is a bullet.
	 * 
	 * @return	True if and only if the space object is a bullet.
	 */
	@Override
	public boolean isBullet() {
		return true;
	}

	/**
	 * Checks whether the bullet has never bounced.
	 * 
	 * @return	True if and only if the bullet has never bounced.
	 * 			| result == (this.getBounceState() == BounceState.INITIAL)
	 */
	public boolean isInitial() {
		return (this.getBounceState() == BounceState.INITIAL);
	}

	/**
	 * Checks whether the bullet has already bounced once.
	 * 
	 * @return	True if and only if the bullet has already bounced one.
	 * 			| result == (this.getBounceState() == BounceState.BOUNCED)
	 */
	public boolean isBounced() {
		return (this.getBounceState() == BounceState.BOUNCED);
	}

	/**
	 * Checks whether the bullet is already terminated.
	 * 
	 * @return	True if and only if the bullet is already terminated.
	 * 			| result == (this.getBounceState() == BounceState.TERMINATED)
	 */
	@Override
	public boolean isTerminated() {
		return (this.getBounceState() == BounceState.TERMINATED);
	}

	/**
	 * Terminates the bullet if it is still existing.
	 *
	 * @post	If the bullet is not yet terminated, detach it from the world it was navigating in
	 * 			and switch its state to terminated. <br> 
	 * 	<code>	| if(!isTerminated()) <br>
	 * 			|    then (!(new this).hasWorld() <br>
	 * 			|		&& (new this).getBounceState == TERMINATED)</code>
	 */
	@Override
	protected void terminate() {
		if (!isTerminated()) {
			this.removeWorld();
			setBounceState(BounceState.TERMINATED);
		}
	}
	
	/**
	 * Checks whether the bullet has the given space object as a source.
	 * 
	 * @return	True if and only if the bullet has the given space object as a source.
	 * 			| result == (spaceObject == this.getSource())
	 */
	@Override
	public boolean hasAsSource(SpaceObject spaceObject) {
		return (spaceObject == this.getSource());
	}

	/**
	 * An enumeration of the possible bouncing states of the bullet.
	 */
	private static enum BounceState {
		INITIAL, BOUNCED, TERMINATED
	}
	
	/**
	 * Returns the bouncing state of the bullet.
	 * 
	 * @return	The bouncing state of the bullet.
	 */
	@Basic
	public BounceState getBounceState() {
		return this.bounceState;
	}
	
	/**
	 * Sets the bouncing state of the bullet to the given bouncing state.
	 * 
	 * @param 	bounceState
	 * 			The given bouncing state
	 */
	private void setBounceState(BounceState bounceState) {
		this.bounceState = bounceState;
	}
	
	/**
	 * Checks whether the bullet has a proper state.
	 * 
	 * @return	True if and only if its state is initial, bounced or terminated.
	 * 			| result == this.isBounced() ^ this.isInitial() ^ this.isTerminated()
	 */
	public boolean hasProperState() {
		return	 this.isBounced()
				^ this.isInitial()
				^ this.isTerminated();
	}
	
	/**
	 * Variable storing the bouncing state of the bullet.
	 * The bouncing state is initialised by getting the state INITIAL.
	 */
	private BounceState bounceState = BounceState.INITIAL;
	
	/**
	 * Makes this bullet collide with another space object.
	 * 
	 * @param	spaceObject
	 * 			The concurrent space object
	 * @effect	If the space object is not a ship or if this bullet doesn't have the space object as a source,
	 * 			both the bullet and the space object explode.
	 * 			| if (!spaceObject.isShip() || !this.hasAsSource(spaceObject))
	 * 			|	then (this.explode() && spaceObject.explode())
	 * @effect	If the space object is a ship and this bullet has the space object as a source,
	 * 			the bullet remember it has collided recently.
	 * 			| if (spaceObject.isShip() && this.hasAsSource(spaceObject))
	 * 			|	then (this.setRecentCollision(true))
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this bullet or the other space object 
	 * 			doesn't have a world attached to it.<br>
	 * 			| (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this bullet and the other space object
	 * 			don't navigate inside the same world.<br>
	 * 			| (!this.hasAsWorld(spaceObject.getWorld()))
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the distance between this bullet
	 * 			and the other space object is not equal to 0.0.<br>
	 * 			| (!Util.fuzzyEquals(this.getDistanceBetween(spaceObject),0.0))
	 * @throws	NullPointerException
	 * 			Will throw an exception if this spaceship or the other space object is <code>null</code>.<br>
	 * 			| (this == null || spaceObject == null)
	 */
	@Override
	public void collide(SpaceObject spaceObject) throws IllegalStateException {
		if (this.hasAsWorld(null) || spaceObject.hasAsWorld(null))
			throw new IllegalStateException();
		if (!this.hasAsWorld(spaceObject.getWorld()))
			throw new IllegalStateException();
		if (!Util.fuzzyEquals(this.getDistanceBetween(spaceObject),0.0))
			throw new IllegalStateException();
		if (spaceObject.isShip() && this.hasAsSource(spaceObject)) {
			this.setRecentCollision(true);
			spaceObject.setRecentCollision(true);
		} else {
			this.explode();
			spaceObject.explode();
		}
	}

	/**
	 * Makes the bullet bounce when hitting a boundary of the world if it had never bounced before,
	 * or terminates the bullet if it had already bounced once.
	 * 
	 * @post	If the bullet is in its initial state and the bullet hits one of the horizontal 
	 * 			boundaries of the world, the new horizontal velocity of the bullet is equal to 
	 * 			the inverted horizontal velocity.<br>
	 * 	<code>	| if(!this.isBounced() && (!this.getWorld().isValidXPosition(this.getXPosition() + this.getRadius())<br>
	 *			|		|| !this.getWorld().isValidXPosition(this.getXPosition() - this.getRadius())))<br>
	 * 			| 	then ((new this).getXVelocity() == -this.getXVelocity()<br>
	 * 			|		&& this.isBounced() == true) </code><br>
	 * @post	If the bullet is in its initial state and the bullet hits one of the vertical 
	 * 			boundaries of the world, the new vertical velocity of the bullet is equal to 
	 * 			the inverted vertical velocity.<br>
	 * 	<code>	| if(!this.isBounced() && (!this.getWorld().isValidYPosition(this.getYPosition() + this.getRadius())<br>
	 *			|		|| !this.getWorld().isValidYPosition(this.getYPosition() - this.getRadius())))<br>
	 *			|	then (new this).getYVelocity() == -this.getYVelocity()<br>
	 * 			|		&& this.isBounced() == true) </code><br>
	 * @post	If the bullet has already bounced once, it is terminated.<br>
	 * 	<code>	| if(this.isBounced())<br>
	 * 			|	then (this.isTerminated() == true) </code>
	 * @throws 	IllegalStateException
	 * 			Will throw an exception if the space object doesn't have a world attached to it
	 * 			or if the space object is terminated.<br>
	 * 	<code>	| (this.hasAsWorld(null) || this.isTerminated()) </code><br>
	 * @throws	IllegalStateException
	 * 			Will throw an exception if the space object could bounce neither horizontally
	 * 			nor vertically. This occurs when the object want to use this method while touching no boundary.
	 * <code><p>| (this.getWorld().isValidXPosition(this.getXPosition() + this.getRadius())<br>
	 *			| 	&& this.getWorld().isValidXPosition(this.getXPosition() - this.getRadius())<br>
	 * 			|	&& this.getWorld().isValidYPosition(this.getYPosition() + this.getRadius())<br>
	 *			|	&& this.getWorld().isValidYPosition(this.getYPosition() - this.getRadius()))</p></code>
	 */
	@Override
	public void bounce() throws IllegalStateException {
		if (this.hasAsWorld(null) || this.isTerminated())
			throw new IllegalStateException();
		if (this.isBounced())
			this.explode();
		else {
			setBounceState(BounceState.BOUNCED);
			super.bounce();
		}
	}
	
	/**
	 * Throws an exception if a bullet is asked to bounce against another space object.
	 * 
	 * @throws	IllegalStateException
	 * 			Will throw an exception if this method is invoked on a bullet,
	 * 			because a bullet can never bounce against another space object.
	 */
	@Override
	protected void bounce(SpaceObject spaceObject) throws IllegalStateException {
		throw new IllegalStateException();
	}

}
