package asteroids.model;

import java.util.*;

import asteroids.util.Util;
import asteroids.view.CollisionListener;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class for dealing with worlds, containing all methods affecting their parameters,
 * such as height and width.<br>
 * It also deals with possible actions for the world, can (un)link the world to (from) an existing space object,
 * makes all the calculations concerning collisions possible and enables it to react 
 * in case of such a collision (by making its space objects bounce, explode or continue their way).
 * 
 * @invar	The height of each world must be a valid height for a world.
 * 			| isValidDimension(getHeight())
 * @invar	The width of each world must be a valid width for a world.
 * 			| isValidDimension(getWidth())
 * @invar	The world must have valid ships.
 * 			| hasProperShips()
 * @invar	The world must have valid asteroids.
 * 			| hasProperAsteroids()
 * @invar	The world must have valid bullets.
 * 			| hasProperBullets()
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.4.3
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-2
 */
public class World {
	
	/**
	 * Initialise the new world with a given height and width, and an empty list of collisions
	 * 
	 * @param	height
	 * 			The height of the new world.
	 * @param 	width
	 * 			The width of the new world.
	 * @post	The height of the new world is equal to the given height.
	 * 			| (new this).getHeight() == height
	 * @post	The width of the new world is equal to the given width.
	 * 			| (new this).getWidth() == width
	 * @post	The upper bound of the new world is equal to MAX_VALUE
	 * 			| (new this).getUpperBound() == Double.MAX_VALUE
	 * @throws	IllegalArgumentException
	 * 			The given height is not a valid height for the world.
	 * 			| (!isValidDimension(height))
	 * @throws	IllegalArgumentException
	 * 			The given width is not a valid width for the world.
	 * 			| (!isValidDimension(width))
	 */
	@Raw
	public World(double height, double width) throws IllegalArgumentException {
		this.upperBound = Double.MAX_VALUE;
		if(!isValidDimension(height))
			throw new IllegalArgumentException("The height is not valid.");
		if(!isValidDimension(width))
			throw new IllegalArgumentException("The width is not valid.");
		this.height = height;
		this.width = width;

	}
	
	/**
	 * Returns the height of the world.
	 * 
	 * @return	The height of the world
	 */
	@Basic @Immutable @Raw
	public double getHeight() {
		return this.height;
	}
	
	/**
	 * Constant storing the height of the world.
	 */
	private final double height;
	
	/**
	 * Returns the width of the world.
	 * 
	 * @return	The width of the world
	 */
	@Basic @Immutable @Raw
	public double getWidth() {
		return this.width;
	}
	
	/**
	 * Constant storing the width of the world.
	 */
	private final double width;
	
	/**
	 * Check if the given dimension is a valid dimension.
	 * 
	 * @param 	dimension
	 * 			The dimension that has to be checked.
	 * @return 	True if and only if the dimension is a positive number and it is smaller than the upper bound.
	 * 			| result == (dimension >= 0) && (dimension <= getUpperBound())
	 */
	public boolean isValidDimension(double dimension) {
		return (dimension >= 0.0) && (dimension <= getUpperBound());
	}
	
	/**
	 * Returns the upper bound of the world.
	 * 
	 * @return	The upper bound of the world
	 */
	@Basic @Immutable
	public double getUpperBound() {
		return this.upperBound;
	}
	
	/**
	 * Constant storing the upper bound for a dimension of a world.
	 */
	private final double upperBound;
		
	/**
	 * Terminates the world if it is still existing.
	 * 
	 * @post   	This world is terminated.
	 *       	| (new this).isTerminated()
	 * @post	The world no longer contains any ships.
	 * 			| (new this).getNbShips() == 0
	 * @post	The world no longer contains any asteroids.
	 * 			| (new this).getNbAsteroids() == 0
	 * @post	The world no longer contains any bullets.
	 * 			| (new this).getNbBullets() == 0
	 * @post   	All ships belonging to this world
	 *         	upon entry, no longer have a world.
	 *       	| for each ship in this.getShips():			//bij prof: each ship in ships  (ships wordt dan abstract gedefinieerd door if-test)
	 *      	|  	((new ship).hasAsWorld(null)) 
	 * @post	All asteroids belonging to this world
	 *       	upon entry, no longer have a world.
	 *       	| for each asteroid in this.getAsteroids():			//bij prof: each asteroid in asteroids  (asteroids wordt dan abstract gedefinieerd door if-test)
	 *       	|  	((new asteroid).hasAsWorld(null)) 	       
	 * @post  	All bullets belonging to this world
	 *        	upon entry, no longer have a world.
	 *       	| for each bullet in this.getBullets():			//bij prof: each bullet in bullets  (bullets wordt dan abstract gedefinieerd door if-test)
	 *       	|  	((new bullet).hasAsWorld(null))        
	 */
	public void terminate() throws IllegalArgumentException, NullPointerException{
		if (!isTerminated()) {
			for (Ship ship : getShips())
				this.removeShip(ship);
			for (Asteroid asteroid : getAsteroids()) 
				this.removeAsteroid(asteroid);
			for (Bullet bullet : getBullets())
				this.removeBullet(bullet);
			isTerminated = true;
		}
	}
		
	/**
	 * Checks if the world is already terminated.
	 */
	@Basic @Raw
	public boolean isTerminated() {
		return this.isTerminated;
	}

	/**
	 * Variable storing the state of the world.
	 */
	private boolean isTerminated;

	/**
	 * Removes the given space object from the space objects of the world.
	 * 
	 * @param 	spaceObject
	 * 			The space object to be removed
	 * @effect	If given space object is a ship, it is added to the world's ships.	
	 *			| if(spaceObject.isShip())
	 * 			| 	then this.addShip(spaceObject)
	 * @effect	If given space object is an asteroid, it is added to the world's asteroids.
	 * 			| if(spaceObject.isAsteroid())
	 * 			| 	then this.addAsteroid(spaceObject)
	 * @effect	If given space object is a bullet, it is added to the world's bullets.
	 * 			| if(spaceObject.isBullet())
	 * 			| 	then this.addBullet(spaceObject)
	 * @throws 	IllegalArgumentException
	 * 			The world does not contain given space object.
	 * 			| !this.hasAsSpaceObject(spaceObject)
	 * @throws	NullPointerException
	 * 			Given space object is null.
	 * 			| spaceObject == null
	 * @throws	IllegalStateException
	 * 			addShip, addAsteroid or AddBullet can throw an IllegalStateException.
	 */
	@Raw
	public void addSpaceObject(SpaceObject spaceObject) 
			throws IllegalArgumentException, IllegalStateException, NullPointerException {
		if (spaceObject.isShip())
			this.addShip((Ship) spaceObject);
		else if (spaceObject.isAsteroid())
			this.addAsteroid((Asteroid) spaceObject);
		else if (spaceObject.isBullet())
			this.addBullet((Bullet) spaceObject);
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Removes the given space object from the space objects of the world.
	 * 
	 * @param 	spaceObject
	 * 			The space object to be removed
	 * @effect	If given space object is a ship, it is removed from the world's ships.	
	 *			| if(spaceObject.isShip())
	 * 			| 	then this.removeShip(spaceObject)
	 * @effect	If given space object is an asteroid, it is removed from the world's asteroids.
	 *  		| if(spaceObject.isAsteroid())
	 * 			| 	then this.removeAsteroid(spaceObject)
	 * @effect	If given space object is a bullet, it is removed from the world's bullets.
	 * 			| if(spaceObject.isBullet())
	 * 			| 	then this.removeBullet(spaceObject)
	 * @throws 	IllegalArgumentException
	 * 			The world does not contain given space object
	 * 			| !this.hasAsSpaceObject(spaceObject)
	 * @throws	NullPointerException
	 * 			Given space object is null.
	 * 			| spaceObject == null
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public void removeSpaceObject(SpaceObject spaceObject) 
			throws IllegalArgumentException, NullPointerException {
		if (spaceObject.isShip())
			this.removeShip((Ship) spaceObject);
		else if (spaceObject.isAsteroid())
			this.removeAsteroid((Asteroid) spaceObject);
		else if (spaceObject.isBullet())
			this.removeBullet((Bullet) spaceObject);
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Add the given ship to the list of ships of the world.		
	 * 
	 * @param  	ship
	 *         	The ship to be added.
	 * @post	The world has given ship as a space object (and vice versa).
	 * 			| (new this).hasAsSpaceObject(new ship)    //echt new?
	 * @post	The number of ships of this world is incremented by 1.
	 * 			| (new this).getNbShips() = this.getNbShips() + 1
	 * @post	If the created ship overlaps an existing bullet, they both explode.
	 * 			| for each bullet in this.getBullets():
	 * 			|	if (ship.overlap(bullet)
	 * 			|		then ((new ship).isTerminated() && (new bullet).isTerminated())
	 * @post	If the created ship overlaps an existing asteroid, the ship explodes.
	 * 			| for each asteroid in getAsteroids():
	 * 			|	if (ship.overlap(asteroid)
	 * 			|		then ((new ship).isTerminated())
	 * @post	If the created ship overlaps an existing ship, the created ship explodes.
	 * 			| for each shipTemp in getShips():
	 * 			|	if (ship.overlap(shipTemp)
	 * 			|		then ((new ship).isTerminated())
	 * @effect	If the world still contains the given ship, its future collisions are calculated.
	 * 			| if((new this).hasAsSpaceObject(new ship))
	 * 			| 	then this.simulateCollision(new ship)
	 * @throws	IllegalArgumentException
	 * 			The world cannot have given ship as a space object.
	 * 			| !this.canHaveAsSpaceObject(ship)
	 * @throws	IllegalArgumentException
	 * 			The world is trying to add a ship that already has a different world as its world.
	 * 			| !ship.hasAsWorld(null) && !ship.hasAsWorld(this)
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public void addShip(Ship ship) throws IllegalArgumentException, IllegalStateException {
		boolean hasToBeTerminated = false;
		if (!this.canHaveAsSpaceObject(ship))
			throw new IllegalArgumentException();
		if (!ship.hasAsWorld(null) && !ship.hasAsWorld(this))
			throw new IllegalArgumentException();
		ships.add(ship);
		if (ship.hasAsWorld(null))					//HIER ECHT NAKIJKEN OF HET WERKT EN OF HET MAG!!!!!
			ship.setWorld(this);
		for (Bullet bullet : getBullets()) {		//beter getBullets()? -> Current...Exception voorkomen
			if (ship.overlap(bullet)) {
				bullet.explode();
				hasToBeTerminated = true;
			}
		}
		for (Asteroid asteroid : getAsteroids()) {	
			if (ship.overlap(asteroid)) {
				hasToBeTerminated = true;
			}
		}
		for (Ship shipTemp : getShips()) {		
			if (ship.overlap(shipTemp)) {
				hasToBeTerminated = true;			
			}
		}
		if (hasToBeTerminated)
			ship.explode();
		else
			simulateCollisions(ship);
	}
	
	/**
	 * Remove the given ship from the list of ships of this world.
	 * 
	 * @param  	ship
	 *        	The ship to be removed.
	 * @post  	The world no longer has the given ship as
	 *       	one of its space objects (and vice versa).
	 *   	 	| !(new this).hasAsSpaceObject(new ship)
	 * @post	The number of ships of this world is decremented by 1.
	 * 			| (new this).getNbShips() = this.getNbShips() - 1
	 * @throws	IllegalArgumentException
	 * 			The world doesn't have given ship as a space object.
	 * 			| !this.hasAsSpaceObject(ship)
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public void removeShip(Ship ship) throws IllegalArgumentException, IllegalStateException {
		if (!this.hasAsSpaceObject(ship))
			throw new IllegalArgumentException();
		ships.remove(ship);
		ship.removeWorld();
		this.removeCollisionsWith(ship);
	}
	
	/**
	 * Add the given asteroid to the list of asteroids of the world.
	 * 
	 * @param  	asteroid
	 *         	The asteroid to be added.
	 * @post	The world has given asteroid as a space object (and vice versa).
	 * 			| (new this).hasAsSpaceObject(new asteroid)
	 * @post	The number of asteroids of this world is incremented by 1.
	 * 			| (new this).getNbAsteroids() = this.getNbAsteroids() + 1
	 * @post	If the created asteroid overlaps an existing bullet, they both explode.
	 * 			| for each bullet in this.getBullets():
	 * 			|	if (asteroid.overlap(bullet)
	 * 			|		then ((new asteroid).isTerminated() && (new bullet).isTerminated())
	 * @post	If the created asteroid overlaps an existing asteroid, the created asteroid explodes.
	 * 			| for each asteroidTemp in this.getAsteroid():
	 * 			|	if (asteroid.overlap(asteroidTemp)
	 * 			|		then ((new asteroid).isTerminated())
	 * @post	If the created asteroid overlaps an existing ship, the existing ship explodes.
	 * 			| for each ship in this.getShips():
	 * 			|	if (asteroid.overlap(ship)
	 * 			|		then ((new ship).isTerminated())
	 * @effect	If the world still contains the given asteroid, its future collisions are calculated.
	 * 			| if((new this).hasAsSpaceObject(new asteroid))
	 * 			| 	then this.simulateCollision(new asteroid)
	 * @throws	IllegalArgumentException
	 * 			The world cannot have given asteroid as a space object.
	 * 			| !this.canHaveAsSpaceObject(asteroid)
	 * @throws	IllegalArgumentException
	 * 			The world is trying to add a ship that already has a different world as its world.
	 * 			| !ship.hasAsWorld(null) && !ship.hasAsWorld(this)
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public void addAsteroid(Asteroid asteroid) throws IllegalArgumentException, IllegalStateException {
		boolean hasToBeTerminated = false;
		if (!canHaveAsSpaceObject(asteroid))
			throw new IllegalArgumentException();
		if (!asteroid.hasAsWorld(null) && !asteroid.hasAsWorld(this))
			throw new IllegalArgumentException();
		asteroids.add(asteroid);
		if (asteroid.hasAsWorld(null))					//HIER ECHT NAKIJKEN OF HET WERKT EN OF HET MAG!!!!!
			asteroid.setWorld(this);
		for (Bullet bullet : getBullets()) {
			if (asteroid.overlap(bullet)) {
				bullet.explode();
				hasToBeTerminated = true;
			}
		}
		for (Asteroid asteroidTemp : getAsteroids()) {
			if (asteroid.overlap(asteroidTemp)) {
				hasToBeTerminated = true;		
			}
		}
		for (Ship ship : getShips()) {
			if (asteroid.overlap(ship)) {
				ship.explode();
			}
		}
		if (hasToBeTerminated)
			asteroid.explode();
		else
			simulateCollisions(asteroid);
	}
	
	/**
	 * Remove the given asteroid from the list of asteroids of this world.
	 * 
	 * @param  	asteroid
	 *        	The asteroid to be removed.
	 * @post  	The world no longer has the given asteroid as
	 *       	one of its space objects (and vice versa).
	 *   	 	| 	! (new this).hasAsSpaceObject(new asteroid)
	 * @post	The number of asteroids of this world is decremented by 1.
	 * 			| (new this).getNbAsteroids() = this.getNbAsteroids() - 1
	 * @throws	IllegalArgumentException
	 * 			The world doesn't have given asteroid as a space object.
	 * 			| !this.hasAsSpaceObject(asteroid)
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public void removeAsteroid(Asteroid asteroid) throws IllegalArgumentException, IllegalStateException, NullPointerException {
		if (!this.hasAsSpaceObject(asteroid))
			throw new IllegalArgumentException();
		asteroids.remove(asteroid);
		asteroid.removeWorld();
		this.removeCollisionsWith(asteroid);
	}
	
	/**
	 * Add the given bullet to the list of bullets of the world.
	 * 
	 * @param  	bullet
	 *         	The bullet to be added.
	 * @post	The world has given bullet as a space object (and vice versa).
	 * 			| (new this).hasAsSpaceObject(new bullet)
	 * @post	The number of bullets of this world is incremented by 1.
	 * 			| (new this).getNbBullets() = this.getNbBullets() + 1
	 * @post	If the created bullet overlaps an existing bullet, they both explode.
	 * 			| for each bulletTemp in this.getBullets():
	 * 			|	if (bullet.overlap(bulletTemp)
	 * 			|		then ((new bullet).isTerminated() && (new bulletTemp).isTerminated())
	 * @post	If the created bullet overlaps an existing asteroid, they both explode.
	 * 			| for each asteroid in this.getAsteroid():
	 * 			|	if (bullet.overlap(asteroid)
	 * 			|		then ((new bullet).isTerminated() && (new asteroid).isTerminated())
	 * @post	If the created bullet overlaps an existing ship and that ship is not the bullet's source, 
	 * 			they both explode.
	 * 			| for each ship in this.getShips():
	 * 			|	if (bullet.overlap(ship) && !bullet.hasAsSource(ship))
	 * 			|		then ((new bullet).isTerminated() && (new ship).isTerminated())
	 * @effect	If the world still contains the given bullet, its future collisions are calculated.
	 * 			| if((new this).hasAsSpaceObject(new bullet))
	 * 			| 	then this.simulateCollision(new bullet)
	 * @throws	IllegalArgumentException
	 * 			The world cannot have given bullet as a space object.
	 * 			| !this.canHaveAsSpaceObject(bullet)
	 * @throws	IllegalArgumentException
	 * 			The world is trying to add a ship that already has a different world as its world.
	 * 			| !ship.hasAsWorld(null) && !ship.hasAsWorld(this)
	 * @throws	NullPointerException
	 * 			| bullet == null
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Raw
	public void addBullet(Bullet bullet) throws IllegalArgumentException, IllegalStateException, NullPointerException {	//misschien toch geen NullPointer (zie eerste test)
		boolean hasToBeTerminated = false;
		if (!canHaveAsSpaceObject(bullet) && isWithinBounds(bullet))	//canHaveAsSpaceObject(bullet) && !isWithinBounds(bullet) gebeurt nooit
			throw new IllegalArgumentException();
//		if (!isWithinBounds(bullet))					//om overlaps nog toe te laten voor bullets out of bounds (onmogelijk met kleine radius zoals nu)
//			hasToBeTerminated = true;
		if (!bullet.hasAsWorld(null) && !bullet.hasAsWorld(this))	//bullet.hasAsWorld(null) && bullet.hasAsWorld(this) kunnen nooit samen true zijn
			throw new IllegalArgumentException();
		bullets.add(bullet);
		if (bullet.hasAsWorld(null)) {			//misschien overbodig, NAKIJKEN!
			bullet.setWorld(this);				//als we een bullet willen herintroduceren in een andere world en we doen het vanuit addBullet en niet vanuit setWorld
		}										//dan wordt de if-test binnengetreden
		for (Bullet bulletTemp : getBullets()) {
			if (bullet.overlap(bulletTemp)) {
				bulletTemp.explode();
				hasToBeTerminated = true;
			}
		}
		for (Asteroid asteroid : getAsteroids()) {
			if (bullet.overlap(asteroid)) {
				asteroid.explode();
				hasToBeTerminated = true;
			}
		}
		for (Ship ship : getShips()) {
			if (bullet.overlap(ship) && !bullet.hasAsSource(ship)) {
				ship.explode();							//als we al een bullet hebben en we willen die opnieuw toevoegen
				hasToBeTerminated = true;							//dan gaat ie zichzelf doen ontploffen? probleem met gelijke identiteit
			}
		}
		if (hasToBeTerminated)
			bullet.explode();
		else
			simulateCollisions(bullet);
	}
	
	/**
	 * Remove the given bullet from the list of bullets of this world.
	 * 
	 * @param  	bullet
	 *        	The bullet to be removed.
	 * @post  	The world no longer has the given bullet as
	 *       	one of its space objects (and vice versa).
	 *   	 	| 	! (new this).hasAsSpaceObject(new bullet)
	 * @post	Given bullet no longer has the world as its world
	 * 			| (new bullet).getWorld() != (new this)
	 * @post	The number of bullets of this world is decremented by 1.
	 * 			| (new this).getNbBullets() = this.getNbBullets() - 1
	 * @throws	IllegalArgumentException
	 * 			The world doesn't have given bullet as a space object.
	 * 			| !this.hasAsSpaceObject(bullet)
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public void removeBullet(Bullet bullet) throws IllegalArgumentException, IllegalStateException {
		if (!this.hasAsSpaceObject(bullet))
			throw new IllegalArgumentException();
		bullets.remove(bullet);
		bullet.removeWorld();
		this.removeCollisionsWith(bullet);
	}
		
	/**
	 * Returns the number of ships stored in the set ships.
	 * 
	 * @return	The number of ships stored in the set ships.
	 * 			| getShips().size()
	 */
	@Raw @Basic
	public int getNbShips() {
		return getShips().size();
	}
	
	/**
	 * Returns the number of asteroids stored in the set asteroids.
	 * 
	 * @return	The number of asteroids stored in the set asteroids
	 * 			| getAsteroids().size()
	 */
	@Raw @Basic
	public int getNbAsteroids() {
		return getAsteroids().size();
	}
	
	/**
	 * Returns the number of bullets stored in the set bullets.
	 * 
	 * @return	The number of bullets stored in the set bullets
	 * 			| getBullets().size()
	 */
	@Raw @Basic
	public int getNbBullets() {
		return getBullets().size();
	}
			
	/**
	 * Checks whether the world has the given space object as one of its space objects and vice versa (bidirectional).
	 * 
	 * @param 	spaceObject
	 * 			The space object to check.
	 * @return	True if and only if the given space object is not null and it has this world as its world
	 * 			and the given space object is saved as a space object of the world.
	 * 			| result == (spaceObject != null && spaceObject.hasAsWorld(this)
	 * 			| 	&& (this.getShips().contains(spaceObject) || this.getAsteroids().contains(spaceObject) || this.getBullets().contains(spaceObject)))
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	public boolean hasAsSpaceObject(@Raw SpaceObject spaceObject) throws IllegalStateException {
		return (spaceObject != null && spaceObject.hasAsWorld(this)
				&& (this.getShips().contains(spaceObject) 
				|| this.getAsteroids().contains(spaceObject) || this.getBullets().contains(spaceObject)));	//kan mislukken, probeer eens later! TESTEN!
																//evt try-catch met IllegalClass ofzo
	}

	/**
	 * Checks whether the world can have the given space object as its space object.
	 * 
	 * @param   spaceObject
	 *          The space object to check.
	 * @return	True if and only if the world is not terminated, the space object is not null,
	 * 			the space object is not terminated and the space object lies within the boundaries
	 * 			of the world.  
	 * 			| result == ((!this.isTerminated()) && (this.isWithinBounds(spaceObject)) 
	 * 			|	&& (spaceObject != null) && (!spaceObject.isTerminated()))
	 */
	@Raw
	public boolean canHaveAsSpaceObject(SpaceObject spaceObject) {
		try {
			return (!this.isTerminated()) && (spaceObject != null) 
				&& (this.isWithinBounds(spaceObject)) && (!spaceObject.isTerminated());
		} catch (NullPointerException exc) {				//echt nodig?
			return false;
		}
	}
		
	/**
	 * Checks whether the world has proper ships attached to it.
	 * 
	 * @return	True if and only if each of these ships references the world as
	 *         	the world to which they are attached
	 *          and if each of these ships lies within the boundaries of the world.
	 *       	|  result ==
	 *       	|   for each ship in ships:
	 *       	|     ( (ship.hasAsWorld(this)) && (isWithinBounds(ship)) )
	 */
	public boolean hasProperShips() {	
//		for (int i = 0; i < getNbSpaceObjects(); i++){
//			if (!canHaveAsSpaceObjectAt(getSpaceObjectAt(i),i))
//				return false;
		for (Ship ship : this.getShips()) {
			if (!ship.hasAsWorld(this))
				return false;
			if (!isWithinBounds(ship))
				return false;
		}
		return true;	
	}
	
	/**
	 * Checks whether the world has proper asteroids attached to it.
	 * 
	 * @return	True if and only if each of these asteroids references the world as
	 *         	the world to which they are attached
	 *          and if each of these asteroids lies within the boundaries of the world.
	 *       	|   result ==
	 *       	|   for each asteroid in asteroids:
	 *       	|     ( (asteroid.hasAsWorld(this)) && (isWithinBounds(asteroid)) )
	 */
	public boolean hasProperAsteroids() {
		for (Asteroid asteroid : this.getAsteroids()) {
			if (!asteroid.hasAsWorld(this))
				return false;
			if (!isWithinBounds(asteroid))
				return false;
		}
		return true;	
	}
	
	/**
	 * Checks whether the world has proper bullets attached to it.
	 * 
	 * @return	True if and only if each of these bullets references the world as
	 *         	the world to which they are attached
	 *          and if each of these bullets lies within the boundaries of the world.
	 *       	|   result ==
	 *       	|   for each bullet in bullets:
	 *       	|     ( (bullet.hasAsWorld(this)) && (isWithinBounds(bullet)) )
	 */
	public boolean hasProperBullets() {
		for (Bullet bullet : this.getBullets()) {
			if (!bullet.hasAsWorld(this))
				return false;
			if (!isWithinBounds(bullet))
				return false;
		}
		return true;	
	}
	
	/**
	 * Returns a copy of the world's ships.
	 * 
	 * @return	A new set of ships, identical to the world's ships.
	 * @return  The size of the resulting set is equal to the number of
	 *          ships of this world.
	 *        | result.size() == getNbShips()
	 */
	@Basic @Immutable
	public Set<Ship> getShips() {
		return new HashSet<Ship>(ships);
	}
	
	/**
	 * Constant set containing the world's ships.
	 * 
	 * @invar	The referenced set is effective.
	 * 			| ships != null
	 * @invar  	Each ship registered in the referenced set is
	 *         	effective and not yet terminated.
	 *       	| for each ship in ships:
	 *       	|   ( (ship != null) &&
	 *       	|     (! ship.isTerminated()) )
	 * @invar 	The ships of a non-terminated world are registered
	 * 			in the set of hashed ships.
	 * 			| for each ship in Ship:
	 * 			| if (! world.isTerminated() && ship.hasAsWorld(world))
	 * 			| then ships.contains(ship)
	 * @note	No invariant to avoid storing the same element twice in the set,
	 * 			because storing each element only once is a property of a set.
	 */
	private final Set<Ship> ships = new HashSet<Ship>();
	
	/**
	 * Returns a copy of the world's asteroids.
	 * 
	 * @return	A new set of asteroids, identical to the world's asteroids.
	 * @return  The size of the resulting set is equal to the number of
	 *          asteroids of this world.
	 *        | result.size() == getNbAstroids()
	 */
	@Basic @Immutable
	public Set<Asteroid> getAsteroids() {
		return new HashSet<Asteroid>(asteroids);
	}
	
	/**
	 * Constant set containing the world's asteroids.
	 * 
	 * @invar	The referenced set is effective.
	 * 			| asteroids != null
	 * @invar  	Each asteroid registered in the referenced set is
	 *         	effective and not yet terminated.
	 *       	| for each asteroid in asteroids:
	 *       	|   ( (asteroid != null) &&
	 *       	|     (! asteroid.isTerminated()) )
	 * @invar 	The asteroids of a non-terminated world are registered
	 * 			in the set of hashed asteroids.
	 * 			| for each asteroid in Asteroid:
	 * 			| if (! world.isTerminated() && asteroid.hasAsWorld(world))
	 * 			| then asteroids.contains(asteroid)
	 */
	private final Set<Asteroid> asteroids = new HashSet<Asteroid>();
	
	/**
	 * Returns a copy of the world's bullets.
	 * 
	 * @return	A new set of bullets, identical to the world's bullets.
	 * @return  The size of the resulting set is equal to the number of
	 *          bullets of this world.
	 *        | result.size() == getNbBullets()
	 */
	@Basic @Immutable
	public Set<Bullet> getBullets() {
		return new HashSet<Bullet>(bullets);
	}
	
	/**
	 * Constant set containing the world's bullets.
	 * 
	 * @invar	The referenced set is effective.
	 * 			| bullets != null
	 * @invar  	Each bullet registered in the referenced set is
	 *         	effective and not yet terminated.
	 *       	| for each bullet in bullets:
	 *       	|   ( (bullet != null) &&
	 *       	|     (! bullet.isTerminated()) )
	 * @invar 	The bullets of a non-terminated world are registered
	 * 			in the set of hashed bullets.
	 * 			| for each bullet in Bullet:
	 * 			| if (! world.isTerminated() && bullet.hasAsWorld(world))
	 * 			| then bullets.contains(bullet)
	 */
	private final Set<Bullet> bullets = new HashSet<Bullet>();
	
	/**
	 * Checks whether the given horizontal position  
	 * is a valid horizontal position within the world on a space object.
	 * 
	 * @param 	xPosition
	 * 			A horizontal position in the Euclidian plane.
	 * @return	True if and only if the horizontal position of a space object lies between 0.0 and the width of this world.
	 * 			| (Util.fuzzyLessThan(xPosition, getWidth()) && Util.fuzzyGreaterThan(xPosition, 0.0))
	 */
	public boolean isValidXPosition(double xPosition){
		return (Util.fuzzyLessThan(xPosition, getWidth()) && Util.fuzzyGreaterThan(xPosition, 0.0));
	}
	
	/**
	 * Checks whether the given vertical position  
	 * is a valid vertical position within the world on a space object.
	 * 
	 * @param 	yPosition
	 * 			A vertical position in the Euclidian plane.
	 * @return	True if and only if the vertical position of a space object lies between 0.0 and the height of this world.
	 * 			| (Util.fuzzyLessThan(yPosition, getHeight()) && Util.fuzzyGreaterThan(yPosition, 0.0))
	 */
	public boolean isValidYPosition(double yPosition){
		return (Util.fuzzyLessThan(yPosition, getHeight()) && Util.fuzzyGreaterThan(yPosition, 0.0));
	}
	
	/**
	 * Checks whether the given space object lies within the boundaries of the world.
	 * 
	 * @param	spaceObject
	 * 			The given space object.
	 * @return	True if and only if the absolute value of 
	 * 			the sum of the horizontal position and the radius is a valid horizontal position for 
	 * 			the given space object in its world, and if the absolute value of 
	 * 			the sum of the vertical position and the radius is a valid vertical position for 
	 * 			the given space object in its world.
	 * 			| result == (isValidXPosition(Math.abs(spaceObject.getXPosition()) + spaceObject.getRadius())
	 * 			|	&& (isValidYPosition(Math.abs(spaceObject.getYPosition()) + spaceObject.getRadius())))
	 * @throws	NullPointerException
	 * 			| (spaceObject == null)
	 */
	@Raw
	public boolean isWithinBounds(SpaceObject spaceObject) throws NullPointerException {	//als we World verschuiven tov de oorsprong, kan dit niet meer kloppen!
		if (!isValidXPosition(spaceObject.getXPosition() + spaceObject.getRadius()))
			return false;
		if (!isValidXPosition(spaceObject.getXPosition() - spaceObject.getRadius()))
			return false;
		if (!isValidYPosition(spaceObject.getYPosition() + spaceObject.getRadius()))
			return false;
		if (!isValidYPosition(spaceObject.getYPosition() - spaceObject.getRadius()))
			return false;
		return true;
	}
	
	/**
	 * Simulates all possible future collisions for given space object and adds those that don't exist yet.
	 * 
	 * @param 	spaceObject
	 * 			The space object for which future collisions will be calculated.
	 * @post	The list of collisions of the world will contain each collision
	 * 			by given space object with one of the world's ships.
	 * 			| for each ship in getShips():
	 * 			|	(getAllCollisions().contains(spaceObject.getNextCollision(ship))
	 * 			|	^ getAllCollisions().contains(ship.getNextCollision(spaceObject)))
	 * @post	The list of collisions of the world will contain each collision
	 * 			by given space object with one of the world's asteroids.
	 * 			| for each asteroid in getAsteroid():
	 * 			|	(getAllCollisions().contains(spaceObject.getNextCollision(asteroid))
	 * 			|	^ getAllCollisions().contains(asteroid.getNextCollision(spaceObject)))
	 * @post	The list of collisions of the world will contain each collision
	 * 			by given space object with one of the world's bullets.
	 * 			| for each bullet in getBullet():
	 * 			|	(getAllCollisions().contains(spaceObject.getNextCollision(bullet))
	 * 			|	^ getAllCollisions().contains(bullet.getNextCollision(spaceObject)))
	 * @post	The list of collisions of the world will contain each collision
	 * 			by given space object with one of the world's boundaries.
	 * 			| getAllCollisions().contains(spaceObject.getNextCollisionToBoundary())
	 * @throws 	NullPointerException
	 * 			| spaceObject == null
	 * @throws	IllegalStateException
	 * 			Will throw an IllegalStateException if one of the internally invoked methods throws it.
	 */
	@Model
	private void simulateCollisions(SpaceObject spaceObject) 
			throws NullPointerException, IllegalStateException {
		
		for (Ship ship: this.getShips()) {
			Collision currentCollision = spaceObject.getNextCollision(ship);
			if(!getAllCollisions().contains(currentCollision.getMirroredCollision())) //evt ook testen op identieke Collision
				allCollisions.add(currentCollision);
		}
		for (Asteroid asteroid: this.getAsteroids()) {
			Collision currentCollision = spaceObject.getNextCollision(asteroid);
			if(!getAllCollisions().contains(currentCollision.getMirroredCollision()))
				allCollisions.add(currentCollision);
		}
		for (Bullet bullet: this.getBullets()) {
			Collision currentCollision = spaceObject.getNextCollision(bullet);
			if(!getAllCollisions().contains(currentCollision.getMirroredCollision()))
				allCollisions.add(currentCollision);
		}
		allCollisions.add(spaceObject.getNextCollisionToBoundary());
	}
	
	/**
	 * Evolves this world and all its space objects for a given duration, 
	 * and resolve all collisions that happened in the meantime, if any.
	 * 
	 * @param	duration
	 * 			The duration for which the world will evolve.
	 * @param	collisionListener
	 * 			
	 * @effect	If the duration is a positive number and if the world has collisions,
	 * 			the collisions are sorted. If the first collision time is smaller than given duration,
	 * 			the space objects of the world evolve for that time, then the first collision occurs
	 * 			and is resolved. Finally the world evolves for the remaining time.
	 * 			If the first collision time is greater than given duration,
	 * 			the space objects of the world evolve for given duration.
	 * 			| if(Util.fuzzyGreaterThan(duration, 0.0) && !getAllCollisions().isEmpty())
	 * 			|	then 
	 * 			|		insertionSort()
	 * 			|		let nextCollision = getAllCollisions().get(0)
	 * 			|			in
	 * 			| 			let newDuration = duration - nextCollision.getTimeToCollision()
	 * 			| 				in
	 * 			| 				if(Util.fuzzyGreaterThan(newDuration, 0.0))
	 * 			|					then (this.evolveSpaceObjects(nextCollision.getTimeToCollision())
	 * 			|						 if (collisionListener != null)
	 * 			|							then nextCollision.toCollisionListener(collisionListener)
	 * 			|						 this.resolveCollision(nextCollision)
	 * 			|						 this.evolve(newDuration, collisionListener))	
	 *			| 				else 
	 *			|					(evolveSpaceObjects(duration))
	 */
	public void evolve(double duration, CollisionListener collisionListener) throws NullPointerException {
		if (Util.fuzzyGreaterThan(duration, 0.0) && (!getAllCollisions().isEmpty())) {
			insertionSort();
			Collision nextCollision = allCollisions.get(0);
			double newDuration = duration - nextCollision.getTimeToCollision();	//als timeToCollision = oneindig => negatieve newDuration => wordt tegengehouden bij volgende test
			if (Util.fuzzyGreaterThan(newDuration, 0.0)) {
				this.evolveSpaceObjects(nextCollision.getTimeToCollision());
				if (collisionListener != null)
					nextCollision.toCollisionListener(collisionListener);
				this.resolveCollision(nextCollision);
			
				this.evolve(newDuration, collisionListener);
			}
			else {
				this.evolveSpaceObjects(duration);
			}		
		}
	}

	/**
	 * Resolves given collision, acting upon the space objects it contains and upon the world's list of collisions appropriately.
	 * 
	 * @param 	collision
	 * 			The collision that occured and has to be resolved.
	 * @post	
	 * 			| let objectOne = collision.getObjectOne()	<br>
	 * 			| let objectTwo = collision.getObjectTwo()	<br>
	 * 			|	in	<br>
	 * 			|	if((new objectOne).isTerminated())	<br>
	 * 			|		then (!this.hasAsSpaceObject(objectOne)	<br>
	 * 			|			 && (for each collision in (new this).getAllCollisions():	<br>
	 * 			|					collision.getObjectOne() != (new objectOne) && collision.getObjectTwo() != (new objectOne)))	<br>
	 *			|	else<br>
	 *			|		then (for each spaceObject in this.getShips(), this.getAsteroids(), this.getBullets():<br>
	 *			|				(this.getAllcollisions().contains((new objectOne).getNextCollision(spaceObject))<br>
	 *			|				^ this.getAllcollisions().contains(spaceObject.getNextCollision(new objectOne)))<br>
	 *			|				&& this.getAllcollisions().contains((new objectTwo).getNextCollisionToBoundary()))<br>
	 * 			|	if((objectTwo != null) && (new objectTwo).isTerminated())<br>
	 * 			|		then (!this.hasAsSpaceObject(objectTwo)<br>
	 * 			|			 && (for each collision in (new this).getAllCollisions():<br>
	 * 			|					collision.getObjectOne() != (new objectTwo) && collision.getObjectTwo() != (new objectTwo)))<br>
	 *			|	else if((objectTwo != null))<br>
	 *			|		then (for each spaceObject in this.getShips(), this.getAsteroids(), this.getBullets():<br>
	 *			|				(this.getAllcollisions().contains((new objectTwo).getNextCollision(spaceObject))<br>
	 *			|				^ this.getAllcollisions().contains(spaceObject.getNextCollision(new objectTwo)))<br>
	 *			|				&& this.getAllcollisions().contains((new objectTwo).getNextCollisionToBoundary()))<br>
	 * @throws	IllegalStateException, NullPointerException<br>
	 * 			Will throw an Exception if one of the internally invoked methods throws one.<br>
	 */
	@Model
	private void resolveCollision(Collision collision) throws NullPointerException, IllegalStateException{
		SpaceObject objectOne = collision.getObjectOne();
		SpaceObject objectTwo = collision.getObjectTwo();
		
		if(objectTwo != null) {
			if (objectOne.isBullet())
				objectOne.collide(objectTwo);
			else if (objectTwo.isBullet())
				objectTwo.collide(objectOne);
			else if (objectOne.isAsteroid())
				objectOne.collide(objectTwo);
			else if (objectTwo.isAsteroid())
				objectTwo.collide(objectOne);
			else 
				objectOne.collide(objectTwo);
			
			if(!objectOne.isTerminated() && !objectTwo.isTerminated() && !objectOne.hasAsSource(objectTwo) && !objectTwo.hasAsSource(objectOne)){
				removeCollisionsWith(objectOne);
				removeCollisionsWith(objectTwo);
				simulateCollisions(objectOne);
				simulateCollisions(objectTwo);
			}
		}
		else {
			if (objectOne != null)	{	//geen else-clause? kan zo mislopen
				objectOne.bounce();
				removeCollisionsWith(objectOne);
				if(!objectOne.isTerminated())
					simulateCollisions(objectOne);	//moet dit binnen?
			}
		}
	}
	
	/**
	 * Removes all collisions between given space object and any other or one the world boundaries
	 * from the world's collisions.
	 * 
	 * @param 	spaceObject
	 * 			The space object whose collisions will be removed from the world's collisions.
	 * @post	
	 * 			| (for each collision in (new this).getAllCollisions():<br>
	 * 			|					collision.getObjectOne() != (spaceObject) && collision.getObjectTwo() != spaceObject)
	 */
	@Model
	private void removeCollisionsWith(SpaceObject spaceObject) {
		for (Collision currentCollision: this.getAllCollisions()) {
			if(currentCollision.getObjectOne() == spaceObject)
				allCollisions.remove(currentCollision);
			if(currentCollision.getObjectTwo() == spaceObject)
				allCollisions.remove(currentCollision);
		}
	}
	
	/**
	 * Makes the space objects of the world evolve for a given duration.
	 * 
	 * @param 	duration
	 * 			The duration for which the space objects will evolve.
	 * @effect	
	 * 			| for each ship in this.getShips():<br>
	 * 			| 	ship.evolve(duration)<br>
	 * 			| for each asteroid in this.getAsteroids():<br>
	 * 			| 	asteroid.evolve(duration)<br>
	 * 			| for each bullet in this.getBullets():<br>
	 * 			| 	bullet.evolve(duration)<br>
	 */
	@Model													
	private void evolveSpaceObjects(double duration) {		
		for (Ship ship : this.getShips())
			ship.evolve(duration);
		for (Asteroid asteroid : this.getAsteroids())
			asteroid.evolve(duration);
		for (Bullet bullet : this.getBullets())
			bullet.evolve(duration);
	}
	
	/**
	 * Returns a copy of the list containing all the world's upcoming collisions()
	 * 
	 * @return	A new list containing all the world's collisions.
	 * @return  The size of the resulting list is equal to the number of
	 *          collisions of this world.
	 *        | result.size() == getNbCollisions()
	 */
	@Basic @Immutable
	public ArrayList<Collision> getAllCollisions() {
		return new ArrayList<Collision>(allCollisions);
	}
	
	/**
	 * Returns the number of saved collisions in the world.<br>
	 * 	
	 * @return	getAllCollisions().size()<br>
	 */
	@Basic @Raw
	public double getNbCollisions() {
		return getAllCollisions().size();
	}
	
	/**
	 * Constant list containing the world's collisions.<br>
	 * 
	 * @invar	The referenced list is effective.<br>
	 * 			| allCollisions != null<br>
	 * @invar  	Each collision registered in the referenced list is<br>
	 *         	effective.<br>
	 *       	| for each collision in allCollisions:<br>
	 *       	|   ( (collision != null)<br>
	 * @invar	Each collision registered in the referenced list appears only once.<br>
	 * 			A collision containing the same space objects, but in reversed order, will not appear.<br> 
	 * 			| for each I,J in 0..allCollisions.size()-1:
	 *			| 	( (I == J)
	 * 			| 	|| (allCollisions.get(I) != allCollisions.get(J))
	 *			| 	|| (allCollisions.get(I) != allCollisions.get(J).getMirroredCollision())
	 */
	private final ArrayList<Collision> allCollisions = new ArrayList<Collision>();

	/**
	 * Sorts the collisions of the world, so that the first to occur is at index 1 and so on.<br>
	 * 
	 * @post	<br>
	 * 			| for each i, j in 0...getNbCollisions():<br>
	 * 			|	if(i < j)<br>
	 * 			|		then (Util.fuzzyLessThanOrEqual(getAllcollisions().get(i).getTimeToCollision(), getAllcollisions().get(j).getTimeToCollision()))<br>
	 *
	 * @invar	| Outer loop:
	 *			|	for each i in 0..getNbCollisions()-1:
	 *			|		allCollisions.get(i).getTimeToCollision() <= allCollisions.get(this.getNbCollisions()-1).getTimeToCollision()
	 * @invar	| Inner loop:
	 *			| 	for each j in 0..i:
	 *			|		allCollisions.get(j).getTimeToCollision() <= allCollisions.get(j+1).getTTC()
	 * @variant	| i starts from 0, increases by 1 during each iteration and will eventually become equal to getNbCollisions()-1
	 * @variant | j starts from i, decreases by 1 during each iteration and will eventually become equal to 0
	 */
	private void insertionSort() {
        boolean finished;
        for (int i = 0 ; i < allCollisions.size(); i++){
            int j = i;
            Collision collision2 = allCollisions.get(j);
            if (i > 0) { 
                double time2 = collision2.getTimeToCollision();

                finished = false;
                while (j != 0 && !finished) {
                	Collision collision1 = allCollisions.get(j-1);
                	double time1 = collision1.getTimeToCollision();
                	if (Util.fuzzyLessThan(time2, time1)) {
                        wissel(j, j-1);	
                    }
                    else {
                        finished = true;
                    }
                    j--;
                }
            }
        }
    }
	
	/**
	 * Interchanges 2 elements of the world's collisions at given indices.<br>
	 * <br>
	 * @param	index2<br>
	 * 			The index of the first collision.<br>
	 * @param 	index1<br>
	 * 			The index of the second collision.<br>
	 * @post	The collision previously found at index1 will be located at index2.<br>
	 * 			The collision previously found at index2 will be located at index1.<br>
	 * 			| (new this).getAllCollisions().get(index2) == this.getAllCollisions().get(index1)<br>
	 * 			| (new this).getAllCollisions().get(index1) == this.getAllCollisions().get(index2)<br>
	 */
	private void wissel(int index2, int index1) {
		Collision collision2 = allCollisions.remove(index2);
		Collision collision1 = allCollisions.remove(index1);
		allCollisions.add(index1, collision2);
		allCollisions.add(index2, collision1);
	}
	
}