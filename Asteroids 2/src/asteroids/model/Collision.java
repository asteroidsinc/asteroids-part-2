package asteroids.model;

import asteroids.view.CollisionListener;
import be.kuleuven.cs.som.annotate.*;

/**
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.4.1
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-2
 *
 */
public class Collision {
	public Collision(SpaceObject objectOne, SpaceObject objectTwo) {
		this.objectOne = objectOne;
		this.objectTwo = objectTwo;
//		if(!isValidTimeToCollision(timeToCollision))
//			throw new IllegalArgumentException();
		//this.timeToCollision = timeToCollision;
	}
	
	public Collision() {
		this(null, null);
	}
	
	
	@Basic @Immutable
	public SpaceObject getObjectOne() {
		return this.objectOne;
	}
	
	@Basic @Immutable
	public SpaceObject getObjectTwo() {
		return this.objectTwo;
	}
	
	/**
	 * 
	 * @return
	 * @throws IllegalStateException
	 */
	public Collision getMirroredCollision() throws IllegalStateException{
		if(this.getObjectTwo() == null)
			throw new IllegalStateException();
		return new Collision(this.getObjectTwo(), this.getObjectOne());
	}
	
	/**
	 * 
	 * @return
	 * @throws IllegalStateException
	 * @throws NullPointerException
	 */
	public double getTimeToCollision() throws IllegalStateException, NullPointerException {
		if (this.getObjectTwo() != null)
			return this.getObjectOne().getTimeToCollision(this.getObjectTwo());
		else
			return this.getObjectOne().getTimeToCollision();
	}
	
	/**
	 * 
	 * @return
	 * @throws IllegalStateException
	 * @throws NullPointerException
	 */
	@Immutable
	public double[] getCollisionPosition() throws IllegalStateException, NullPointerException{
		if (this.getObjectTwo() != null)
			return this.getObjectOne().getCollisionPosition(this.getObjectTwo());
		else
			return this.getObjectOne().getCollisionPosition();
	}
	
	/**
	 * 
	 * @param collisionListener
	 * @throws IllegalStateException
	 * @throws NullPointerException
	 */
	public void toCollisionListener(CollisionListener collisionListener) throws IllegalStateException, NullPointerException{
		if(this.getObjectTwo() != null) {
			if (!this.getObjectOne().hasAsSource(this.getObjectTwo()) && !this.getObjectTwo().hasAsSource(this.getObjectOne())){
				collisionListener.objectCollision(getObjectOne(), getObjectTwo(), 
				getCollisionPosition()[0], getCollisionPosition()[1]);
			}	
		}
		else {
			collisionListener.boundaryCollision(getObjectOne(), 
					getCollisionPosition()[0], getCollisionPosition()[1]);
		}
	}
	
	private final SpaceObject objectOne;
	
	private final SpaceObject objectTwo;

}
