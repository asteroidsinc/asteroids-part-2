package asteroids.util;


/**
 * A class for dealing with vectors, containing all methods necessary for vector operations.
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.3.2
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-2
 */
public class Vector {		//nog niet consequent in formele commentaren
	
	/**
	 * Initialise the new vector with a given horizontal and vertical coordinate.
	 * 
	 * @param 	xCoord
	 * 			The horizontal coordinate of the new vector.
	 * @param 	yCoord
	 * 			The vertical coordinate of the new vector.
	 * @post	The horizontal coordinate of the new vector is equal to the given horizontal coordinate.
	 * 		  	| (new this).X_COORD == xCoord
	 * @post	The vertical coordinate of the new vector is equal to the given vertical coordinate.
	 * 		  	| (new this).Y_COORD == yCoord
	 */
	public Vector(double xCoord, double yCoord){
		if(Double.isNaN(xCoord)) 
			xCoord = 0;
		this.X_COORD = xCoord;
		if(Double.isNaN(yCoord)) 
			yCoord = 0;
		this.Y_COORD = yCoord;
	}
	
	/**
	 * Initialise the new vector with a given horizontal and vertical coordinate.
	 * 
	 * @post	The horizontal coordinate of the new vector is equal to zero.
	 * 		  	| (new this).X_COORD == 0
	 * @post	The vertical coordinate of the new vector is equal to zero.
	 * 		  	| (new this).Y_COORD == 0
	 */
	public Vector(){
		this(0,0);
	}
	
	/**
	 * Constant storing the horizontal coordinate of the vector.
	 */
	public final double X_COORD;
	
	/**
	 * Constant storing the vertical coordinate of the vector.
	 */
	public final double Y_COORD;
	
	/**
	 * Returns an array (of appropriate length) containing the vector components of given vector.
	 * 
	 * @param 	vector
	 * 			The vector that is converted to an array.
	 * @return	The array containing the horizontal and vertical components of the vector.
	 * 			| result[0] == vector.X_COORD
	 * 			| result[1] == vector.Y_COORD
	 */
	public static double[] arrayVector(Vector vector){
		double[] array = new double[2];
		array[0] = vector.X_COORD;
		array[1] = vector.Y_COORD;
		return array;
	}
	
	/**
	 * Return a new vector with a given magnitude and angle.
	 * 
	 * @param 	magnitude
	 * 			The magnitude of the new vector.
	 * @param 	angle
	 * 			The angle of the new vector, measured counterclockwise from the positive x-axis.
	 * @return	The new vector with given parameters.
	 * 			| result == new Vector(magnitude * Math.cos(angle), magnitude * Math.sin(angle))
	 */
	public static Vector polarVector(double magnitude, double angle){
		if(Double.isNaN(magnitude)) 
			magnitude = 0;
		if(Double.isNaN(angle)) 
			angle = 0;
		return new Vector(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	}

	/**
	 * Returns a vector opposite to given vector.
	 * 
	 * @param 	v1
	 * 			The vector of which we seek the opposite.
	 * @return	The vector opposite of the given vector.
	 * 			| result == multiple(v1, -1)
	 */
	public static Vector opposite(Vector v1){
		return multiple(v1, -1);
	}
	
	/**
	 * Returns the vector resulting from the vectorial addition of 2 given vectors.
	 * 
	 * @param 	v1
	 * 			The first vector of the operation. 
	 * @param 	v2
	 * 			The second vector of the operation.
	 * @return	The vectorial sum of the 2 vectors.
	 * 			| result == new Vector(v1.X_COORD + v2.X_COORD, v1.Y_COORD + v2.Y_COORD)
	 */
	public static Vector addition(Vector v1, Vector v2){
		return new Vector(v1.X_COORD + v2.X_COORD, v1.Y_COORD + v2.Y_COORD);
	}
	
	/**
	 * Returns the vector resulting from the substraction of the second vector
	 * from the first.
	 * 
	 * @param 	v1
	 * 			The vector from which the second vector is substracted.
	 * @param 	v2
	 * 			The vector substracted from the first vector.
	 * @return	The vector resulting from the extraction.
	 * 			| result == addition(v1, opposite(v2))
	 */
	public static Vector substraction(Vector v1, Vector v2){
		return addition(v1, opposite(v2));
	}
	
	/**
	 * Returns a multiple of the given vector, with given multiplyign factor.
	 * 
	 * @param 	v1
	 * 			The vector we want to multiply.
	 * @param 	factor
	 * 			The factor of the multiplication.
	 * @return	The multiple of given vector, by the given factor.
	 * 			| result == new Vector(factor * v1.X_COORD, factor * v1.Y_COORD)
	 */
	public static Vector multiple(Vector v1, double factor){
		return new Vector(factor * v1.X_COORD, factor * v1.Y_COORD);
	}
	
	/**
	 * Returns the norm of the given vector.
	 * 
	 * @param 	v1
	 * 			The vector of which we seek the norm.
	 * @return	The norm of the vector.
	 * 			| result == Math.sqrt(dotProduct(v1, v1))
	 */
	public static double norm(Vector v1){
		return Math.sqrt(dotProduct(v1, v1));
	}
	
	/**
	 * Returns the dot product of 2 given vectors.
	 * 
	 * @param 	v1
	 * 			The first vector of the operation.
	 * @param 	v2
	 * 			The second vector of the operation.
	 * @return	The dot product of the 2 vectors.
	 * 			| result == v1.X_COORD*v2.X_COORD + v1.Y_COORD*v2.Y_COORD
	 */
	public static double dotProduct(Vector v1, Vector v2){
		return v1.X_COORD * v2.X_COORD + v1.Y_COORD * v2.Y_COORD;
	}
	
	/**
	 * Returns the distance between 2 given vectors.
	 * 
	 * @param 	v1
	 * 			The first vector of the operation.
	 * @param 	v2
	 * 			The second vector of the operation.
	 * @return	The distance between the 2 vectors.
	 * 			| result == norm(substraction(v1, v2))
	 */
	public static double distance(Vector v1, Vector v2){
		return norm(substraction(v1, v2));
	}
}
