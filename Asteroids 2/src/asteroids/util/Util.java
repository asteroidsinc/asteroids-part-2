package asteroids.util;

/**
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.3.3
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-2
 */

public class Util {
  public static final double EPSILON = 0.0001;

  public static boolean fuzzyEquals(double x, double y) {
    if (Double.isNaN(x) || Double.isNaN(y))
      return false;
    return Math.abs(x - y) <= EPSILON || Double.valueOf(x).equals(Double.valueOf(y));
  }

  public static boolean fuzzyLessThanOrEqualTo(double x, double y) {
    if (fuzzyEquals(x, y)) {
      return true;
    } else {
      return Double.compare(x, y) < 0;
    }
  }
  
  public static boolean fuzzyGreaterThanOrEqualTo(double x, double y) {
	  return fuzzyLessThanOrEqualTo(y, x);
  }
  
  public static boolean fuzzyLessThan(double x, double y) {
	  return !fuzzyLessThanOrEqualTo(y, x);
  }
  
  public static boolean fuzzyGreaterThan(double x, double y) {
	  return !fuzzyLessThanOrEqualTo(x, y);
  }
  
  public static double absoluteError(double expected, double actual) {
    return Math.abs(expected - actual);
  }

  public static double relativeError(double expected, double actual) {
    return absoluteError(expected, actual) / Math.abs(expected);
  }
  
  public static double minimum(double a, double b) {
	  if (Double.isNaN(a) || a < 0.0)				//misschie fuzzyLeq
		  a = Double.POSITIVE_INFINITY;
	  if (Double.isNaN(b) || b < 0.0)
		  b = Double.POSITIVE_INFINITY;	
	  if (a > b)
		  return b;
	  else 
		  return a;
  }
}