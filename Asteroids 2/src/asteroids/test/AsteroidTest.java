package asteroids.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asteroids.model.Asteroid;

public class AsteroidTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	
	@Test
	public final void isAsteroid_SingleCase() {
		Asteroid asteroid = new Asteroid();
		assertTrue(asteroid.isAsteroid());
	}
	
	@Test
	public final void isShip_SingleCase() {
		Asteroid asteroid = new Asteroid();
		assertFalse(asteroid.isShip());
	}
	
	@Test
	public final void isBullet_SingleCase() {
		Asteroid asteroid = new Asteroid();
		assertFalse(asteroid.isBullet());
	}
}
