package asteroids.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asteroids.control.IFacade;
import asteroids.exception.ModelException;
import asteroids.model.*;
import asteroids.util.Util;

public class PartialFacadeTest {

  IFacade<World, Ship, Asteroid, Bullet> facade;
  
  @Before
  public void setUp() {
    facade = new Facade();
  }
  
  @Test
  public void testCreateShip() {
    Ship ship = facade.createShip(100, 200, 10, -10, 20, -Math.PI, 1.5E5);
    assertNotNull(ship);
    assertEquals(100, facade.getShipX(ship), Util.EPSILON);
    assertEquals(20, facade.getShipRadius(ship), Util.EPSILON);
  }

  @Test(expected=ModelException.class)
  public void testCreateShipXIsNan() {
	  facade.createShip(Double.NaN, 200, 10, -10, 20, -Math.PI, 4.6E8);
  }
  
  @Test(expected=ModelException.class)
  public void testCreateShipRadiusNegative() {
	  facade.createShip(100, 200, 10, -10, -20, -Math.PI, 7.2E2);
  }
  
}
