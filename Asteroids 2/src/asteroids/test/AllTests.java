package asteroids.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ AsteroidTest.class, PartialFacadeTest.class,
	ShipTest.class, VectorTest.class, WorldTest.class })

public class AllTests {
	
}

