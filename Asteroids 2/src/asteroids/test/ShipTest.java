
package asteroids.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asteroids.model.*;
import asteroids.util.Util;
import asteroids.util.Vector;

/**
 * A class for testing the functionalities of the class Ship.
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.4.3 (pre 3.0)
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-2
 */
public class ShipTest {

	private Ship	ship0_0_0_10_60_13_45E4, 		ship0_0_min15_0_157_50_61,		ship0_0_0_min10_45_15_20E6, 	
					ship0_30_10_0_108_15_73E2,		ship0_34_0_min10_135_11_13E4,	ship0_30_0_min20_0_15_29E10,
					ship20_0_0_10_0_13_98E3,		ship40_min10_10_0_0_12_1,		ship60_30_15_20_113_16_52E6,
					ship40_50_min10_20_180_15_4E2,	ship100_100_min10_min20_135_20_6E4,
					ship150_70_15_30_90_10_8E14,	ship200_125_min25_5_0_25_5E5,	ship600_400_20_10_180_20_7E2,
					ship30_10_20_20_0_10_5E3,		ship70_100_min10_20_45_10_5E10,	ship100_100_min10_min10_min135_10_7E13;
	
	private World	world900_1440,					world800_1280,					world1000_1600;
					
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		ship0_0_0_10_60_13_45E4 = new Ship(0,0,0,10,Math.PI/3,13,4.5E5);
		ship0_0_0_min10_45_15_20E6 = new Ship(0,0,0,-10,Math.PI/4,15,2.0E7);
		ship0_0_min15_0_157_50_61 = new Ship(0,0,-15,0,7*Math.PI/8,50,6.1E1);
		ship0_30_0_min20_0_15_29E10 = new Ship(0,30,0,-20,0,15,2.9E11);
		ship0_30_10_0_108_15_73E2 = new Ship(0,30,10,0,3*Math.PI/5,15,7.3E3);
		ship20_0_0_10_0_13_98E3 = new Ship(20,0,0,10,0,13,9.8E4);
		ship0_34_0_min10_135_11_13E4 = new Ship(0,34,0,-10,3*Math.PI/4,11,1.3E5);
		ship40_min10_10_0_0_12_1 = new Ship(40,-10,10,0,0,12,1);
		ship60_30_15_20_113_16_52E6 = new Ship(60,30,15,20,5*Math.PI/8,16,5.2E7);
		ship40_50_min10_20_180_15_4E2 = new Ship(40,50,-10,20,Math.PI,15,4E2);
		ship100_100_min10_min20_135_20_6E4 = new Ship(100,100,-10,-20,3*Math.PI/4,20,6E4);
		ship70_100_min10_20_45_10_5E10 = new Ship(70,100,-10,20,Math.PI/4,10,5E10);
		ship150_70_15_30_90_10_8E14 = new Ship(150,70,15,30,Math.PI/2,10,8E14);
		ship200_125_min25_5_0_25_5E5 = new Ship(200,125,-25,5,0,25,5E5);
		ship600_400_20_10_180_20_7E2 = new Ship(600,400,20,10,Math.PI,20,7E2);
		ship30_10_20_20_0_10_5E3 = new Ship(30,10,20,20,0,10,5E3);
		ship100_100_min10_min10_min135_10_7E13 = new Ship(100,100,-10,-10,-3*Math.PI/4,10,7E13);
		world900_1440 = new World(900,1440);
		world800_1280 = new World(800,1280);
		world1000_1600 = new World(1000,1600);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void Constructor_LegalCase() throws Exception {
		Ship ship = new Ship(0,0,-10*Math.sqrt(2),10*Math.sqrt(2),2*Math.PI/3,25,8.5E2);
		assert Util.fuzzyEquals(0, ship.getXPosition());
		assert Util.fuzzyEquals(0, ship.getYPosition());
		assert Util.fuzzyEquals(-10*Math.sqrt(2), ship.getXVelocity());
		assert Util.fuzzyEquals(10*Math.sqrt(2), ship.getYVelocity());
		assert Util.fuzzyEquals(2*Math.PI/3, ship.getDirection());
		assert Util.fuzzyEquals(25, ship.getRadius());
		assert Util.fuzzyEquals(8.5E2, ship.getMass());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void Constructor_IllegalCaseXPosition() throws Exception {
		new Ship(Double.NaN,10,25,-25,Math.PI,20,6.3E8);
	}

	@Test(expected = IllegalArgumentException.class)
	public final void Constructor_IllegalCaseYPosition() throws Exception {
		new Ship(10,Double.NaN,25,-25,Math.PI,20,6.3E8);
	}

	@Test
	public final void Constructor_IllegalCaseVelocity() throws Exception {
		Ship ship = new Ship(50,50,250000,250000,Math.PI/2,15,9.4E2);
		assert Util.fuzzyEquals(ship.getSpeedLimit()/Math.sqrt(2), ship.getXVelocity());
		assert Util.fuzzyEquals(ship.getSpeedLimit()/Math.sqrt(2), ship.getYVelocity());
	}
	
	@Test
	public final void Constructor_IllegalCaseDirection() throws Exception {
		assertFalse(Ship.isValidDirection(Double.NaN));
	}

	@Test(expected = IllegalArgumentException.class)
	public final void Constructor_IllegalCaseRadius() throws Exception {
		new Ship(50,50,-10*Math.sqrt(2),10*Math.sqrt(2),3*Math.PI/4,-2,5.2E4);		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void Constructor_IllegalCaseMass() throws Exception {
		new Ship(50,50,10,12,4*Math.PI/7,14,-4.5E4);
	}
		
	@Test
	public final void isValidVelocity_TrueCase() {
		Ship ship = new Ship();
		assertTrue (ship.isValidVelocity(new Vector(0,-10)));
		assertTrue (ship.isValidVelocity(new Vector(-15,0)));
		assertTrue (ship.isValidVelocity(new Vector(10,60)));
		assertTrue (ship.isValidVelocity(new Vector(Double.NaN,54)));
		assertTrue (ship.isValidVelocity(new Vector(20,Double.NaN)));
	}
	
	@Test
	public final void isValidVelocity_FalseCase() {
		Ship ship = new Ship();
		assertFalse (ship.isValidVelocity(new Vector(Double.POSITIVE_INFINITY,231)));
		assertFalse (ship.isValidVelocity(new Vector(72,Double.POSITIVE_INFINITY)));
		assertFalse (ship.isValidVelocity(new Vector(Double.NEGATIVE_INFINITY,231)));
		assertFalse (ship.isValidVelocity(new Vector(72,Double.NEGATIVE_INFINITY)));
	}

	@Test
	public final void isValidDirection_PositiveCase() {
		assertTrue(Ship.isValidDirection(7));
	}

	@Test 
	public final void isValidDirection_NegativeCase() {
		assertTrue(Ship.isValidDirection(-5));
	}
	
	@Test
	public final void isValidDirection_FalseCase() {
		assertFalse(Ship.isValidDirection(Double.NaN));
	}
	
	@Test
	public final void isValidRadius_TrueCase() {
		Ship ship = new Ship();
		assertTrue(ship.isValidRadius(26));
		assertTrue(ship.isValidRadius(10));
		assertTrue(ship.isValidRadius(Double.POSITIVE_INFINITY));
	}

	@Test
	public final void isValidRadius_NegativeCase() {
		Ship ship = new Ship();
		assertFalse(ship.isValidRadius(0));
		assertFalse(ship.isValidRadius(-9));
	}
	
	@Test
	public final void isValidRadius_NaNCase() {
		Ship ship = new Ship();
		assertFalse(ship.isValidRadius(Double.NaN));
	}
	
	@Test
	public final void isValidMass_TrueCase() {
		assertTrue(Ship.isValidMass(26));
		assertTrue(Ship.isValidMass(10));
		assertTrue(Ship.isValidMass(Double.POSITIVE_INFINITY));
	}

	@Test
	public final void isValidMass_NegativeCase() {
		assertFalse(Ship.isValidMass(0));
		assertFalse(Ship.isValidMass(-9));
	}
	
	@Test
	public final void isValidMass_NaNCase() {
		assertFalse(Ship.isValidMass(Double.NaN));
	}
	
	@Test
	public final void canHaveAsWorld_TrueCase() {
		assertTrue(ship60_30_15_20_113_16_52E6.canHaveAsWorld(world900_1440));
	}
	
	@Test
	public final void canHaveAsWorld_WorldNullCase() {
		assertTrue(ship60_30_15_20_113_16_52E6.canHaveAsWorld(null));
	}
	
	@Test
	public final void canHaveAsWorld_SpaceObjectTerminatedCase() {
		ship60_30_15_20_113_16_52E6.explode();
		assertFalse(ship60_30_15_20_113_16_52E6.canHaveAsWorld(world900_1440));
	}
	
	@Test
	public final void canHaveAsWorld_WorldTerminatedCase() {
		world900_1440.terminate();
		assertFalse(ship60_30_15_20_113_16_52E6.canHaveAsWorld(world900_1440));
	}
	
	@Test
	public final void canHaveAsWorld_SpaceObjectOutOfBoundsCase() {
		ship0_0_0_10_60_13_45E4.canHaveAsWorld(world900_1440);
	}

	@Test
	public final void setWorld_LegalCase() throws Exception {
		ship60_30_15_20_113_16_52E6.setWorld(world800_1280);
		assertEquals(world800_1280, ship60_30_15_20_113_16_52E6.getWorld());
		ship100_100_min10_min20_135_20_6E4.setWorld(world1000_1600);
		assertEquals(world1000_1600,ship100_100_min10_min20_135_20_6E4.getWorld());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void setWorld_CannotHaveAsWorldCase() throws Exception {
		ship60_30_15_20_113_16_52E6.explode();
		ship60_30_15_20_113_16_52E6.setWorld(world1000_1600);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void setWorld_AlreadyAWorldCase() throws Exception {
		ship60_30_15_20_113_16_52E6.setWorld(world800_1280);
		ship60_30_15_20_113_16_52E6.setWorld(world900_1440);
	}
	
	@Test
	public final void setWorld_WorldNullCase() throws Exception {
		ship60_30_15_20_113_16_52E6.setWorld(null);
		assertNull(ship60_30_15_20_113_16_52E6.getWorld());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void setWorld_InitialContactCase() throws Exception {
		ship30_10_20_20_0_10_5E3.setWorld(world800_1280);
	}
	
	@Test
	public final void hasAsWorld_ThisTerminatedCase() throws Exception {
		ship60_30_15_20_113_16_52E6.setWorld(world1000_1600);
		ship60_30_15_20_113_16_52E6.explode();
		assertFalse(!ship60_30_15_20_113_16_52E6.hasAsWorld(null));
	}
	
	@Test
	public final void hasAsWorld_WorldNullCase() throws Exception {
		assertFalse(!ship60_30_15_20_113_16_52E6.hasAsWorld(null));
	}
	
	@Test
	public final void removeWorld_LegalCase() throws Exception {
		ship40_50_min10_20_180_15_4E2.setWorld(world800_1280);
		ship40_50_min10_20_180_15_4E2.removeWorld();
		assertNull(ship40_50_min10_20_180_15_4E2.getWorld());
	}
	
	@Test
	public final void explode_AlreadyTerminatedCase() throws Exception {
		ship0_30_10_0_108_15_73E2.explode();
		assertTrue(ship0_30_10_0_108_15_73E2.isTerminated());
		ship0_30_10_0_108_15_73E2.explode();					//om te checken wat er gebeurt wanneer al geterminated
		assertTrue(ship0_30_10_0_108_15_73E2.isTerminated());
	}
	
	@Test
	public final void isShip_SingleCase() {
		Ship ship = new Ship();
		assertTrue(ship.isShip());
	}
	
	@Test
	public final void isAsteroid_SingleCase() {
		Ship ship = new Ship();
		assertFalse(ship.isAsteroid());
	}
	
	@Test
	public final void isBullet_SingleCase() {
		Ship ship = new Ship();
		assertFalse(ship.isBullet());
	}
	
	@Test
	public final void move_LegalCase() throws Exception {
		ship150_70_15_30_90_10_8E14.setWorld(world1000_1600);
		ship150_70_15_30_90_10_8E14.move(5);
		assert Util.fuzzyEquals(225, ship150_70_15_30_90_10_8E14.getXPosition());
		assert Util.fuzzyEquals(220, ship150_70_15_30_90_10_8E14.getYPosition());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void move_NegativeDurationCase() throws Exception {
		ship0_0_0_10_60_13_45E4.move(-1);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void move_TerminatedCase() throws Exception {
		ship0_0_0_10_60_13_45E4.explode();
		ship0_0_0_10_60_13_45E4.move(5);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void move_OutOfBoundsDurationCase() {
		ship150_70_15_30_90_10_8E14.setWorld(world800_1280);
		ship150_70_15_30_90_10_8E14.move(100);
	}
	
	@Test
	public final void isValidDuration_TrueCase() {
		assertTrue (Ship.isValidDuration(0));
		assertTrue (Ship.isValidDuration(6333212));
	}
	
	@Test
	public final void isValidDuration_FalseCase() {
		assertFalse (Ship.isValidDuration(-4));
		assertFalse (Ship.isValidDuration(Double.NaN));
	}
	
	@Test
	public final void turn_PositiveAngle() {
		ship0_0_0_10_60_13_45E4.turn(Math.PI);
		assert Util.fuzzyEquals(4*Math.PI/3, ship0_0_0_10_60_13_45E4.getDirection());
	}
	
	@Test
	public final void turn_NegativeAngle() {
		ship0_34_0_min10_135_11_13E4.turn(-3*Math.PI/2);
		assert Util.fuzzyEquals(-3*Math.PI/4, ship0_34_0_min10_135_11_13E4.getDirection());
	}
	
	@Test
	public final void getDistanceBetween_PositiveCase() throws Exception {
		assert Util.fuzzyEquals(2, ship0_0_0_10_60_13_45E4.getDistanceBetween(ship0_30_0_min20_0_15_29E10));
	}
	
	@Test
	public final void getDistanceBetween_OuterZeroCase() throws Exception {
		assert Util.fuzzyEquals(0, ship0_0_0_min10_45_15_20E6.getDistanceBetween(ship0_30_0_min20_0_15_29E10));
	}
	
	@Test
	public final void getDistanceBetween_SmallOverlap() throws Exception {
		assert Util.fuzzyEquals(-6, ship0_0_0_10_60_13_45E4.getDistanceBetween(ship20_0_0_10_0_13_98E3));
	}
	
	@Test
	public final void getDistanceBetween_InnerZeroCase() throws Exception {
		assert Util.fuzzyEquals(-22, ship0_34_0_min10_135_11_13E4.getDistanceBetween(ship0_30_0_min20_0_15_29E10));
	}
	
	@Test
	public final void getDistanceBetween_InsideCase() throws Exception {
		assert Util.fuzzyEquals(-43, ship0_0_min15_0_157_50_61.getDistanceBetween(ship20_0_0_10_0_13_98E3));
	}
	
	@Test
	public final void getDistanceBetween_Concentric() throws Exception {
		assert Util.fuzzyEquals(-28, ship0_0_0_min10_45_15_20E6.getDistanceBetween(ship0_0_0_10_60_13_45E4));   
		assert Util.fuzzyEquals(-28, ship0_0_0_10_60_13_45E4.getDistanceBetween(ship0_0_0_min10_45_15_20E6));
	}
	
	@Test(expected = NullPointerException.class)
	public final void getDistanceBetween_NullPointerCase() throws Exception {
		ship0_0_0_min10_45_15_20E6.getDistanceBetween(null);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getDistanceBetween_ThisTerminatedCase() throws Exception {
		ship0_0_0_min10_45_15_20E6.explode();
		ship0_0_0_min10_45_15_20E6.getDistanceBetween(ship0_34_0_min10_135_11_13E4);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getDistanceBetween_OtherTerminatedCase() throws Exception {
		ship0_34_0_min10_135_11_13E4.explode();
		ship0_0_0_min10_45_15_20E6.getDistanceBetween(ship0_34_0_min10_135_11_13E4);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getDistanceBetween_DifferentWorldCase() throws Exception {
		ship40_50_min10_20_180_15_4E2.setWorld(world800_1280);
		ship60_30_15_20_113_16_52E6.setWorld(world1000_1600);
		ship40_50_min10_20_180_15_4E2.getDistanceBetween(ship60_30_15_20_113_16_52E6);
	}
	
	@Test
	public final void overlap_NormalCase() throws Exception {
		assertFalse (ship0_0_0_10_60_13_45E4.overlap(ship0_30_0_min20_0_15_29E10));
		assertFalse (ship0_30_0_min20_0_15_29E10.overlap(ship0_0_0_10_60_13_45E4));
	}
	
	@Test
	public final void overlap_NormalOverlapCase() throws Exception {
		assertTrue (ship0_0_0_10_60_13_45E4.overlap(ship20_0_0_10_0_13_98E3));
		assertTrue (ship20_0_0_10_0_13_98E3.overlap(ship0_0_0_10_60_13_45E4));
	}
	
	@Test
	public final void overlap_SelflOverlapCase() throws Exception {
		assertFalse (ship0_0_0_10_60_13_45E4.overlap(ship0_0_0_10_60_13_45E4));
	}
	
	@Test(expected = NullPointerException.class)
	public final void overlap_IllegalCase() throws Exception {
		ship0_0_0_min10_45_15_20E6.overlap(null);
	}
	
	@Test
	public final void getTimeToCollision_WithArgs_CollisionCase() throws Exception {
		assert Util.fuzzyEquals(0.584275594, ship20_0_0_10_0_13_98E3.getTimeToCollision(ship0_30_10_0_108_15_73E2));
	}
	
	@Test
	public final void getTimeToCollision_WithArgs_OverlapCase() throws Exception {
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_30_0_min20_0_15_29E10.getTimeToCollision(ship0_34_0_min10_135_11_13E4)); 	//beginnen in overlap en gaan uit elkaar
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_30_10_0_108_15_73E2.getTimeToCollision(ship0_34_0_min10_135_11_13E4));
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getTimeToCollision_WithArgs_InitialContactCase() throws Exception {
		ship100_100_min10_min20_135_20_6E4.setWorld(world900_1440);
		ship70_100_min10_20_45_10_5E10.setWorld(world900_1440);
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship100_100_min10_min20_135_20_6E4.getTimeToCollision(ship70_100_min10_20_45_10_5E10));		//werkt niet ideaal, hoe zou het moeten?
	}
	
	@Test
	public final void getTimeToCollision_WithArgs_ParallelCase() throws Exception {
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_30_10_0_108_15_73E2.getTimeToCollision(ship40_min10_10_0_0_12_1));
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship40_min10_10_0_0_12_1.getTimeToCollision(ship0_30_10_0_108_15_73E2));
	}
	
	@Test
	public final void getTimeToCollision_WithArgs_DivergingCase() throws Exception {
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_0_0_min10_45_15_20E6.getTimeToCollision(ship40_min10_10_0_0_12_1));
	}
	
	@Test
	public final void getTimeToCollision_WithArgs_DNegative() throws Exception {
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_34_0_min10_135_11_13E4.getTimeToCollision(ship40_min10_10_0_0_12_1));
	}
	
	@Test
	public final void getTimeToCollision_WithArgs_SelfCollideCase() throws Exception {
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, ship0_30_0_min20_0_15_29E10.getTimeToCollision(ship0_30_0_min20_0_15_29E10));
	}
	
	@Test(expected = NullPointerException.class)
	public final void getTimeToCollision_WithArgs_NullPointerCase() throws Exception  {
		ship0_0_0_min10_45_15_20E6.getTimeToCollision(null);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getTimeToCollision_WithArgs_ThisTerminatedCase() throws Exception {
		ship100_100_min10_min20_135_20_6E4.setWorld(world900_1440);
		ship40_50_min10_20_180_15_4E2.setWorld(world900_1440);
		ship100_100_min10_min20_135_20_6E4.explode();
		ship100_100_min10_min20_135_20_6E4.getTimeToCollision(ship40_50_min10_20_180_15_4E2);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getTimeToCollision_WithArgs_OtherTerminatedCase() throws Exception {
		ship100_100_min10_min20_135_20_6E4.setWorld(world900_1440);
		ship40_50_min10_20_180_15_4E2.setWorld(world900_1440);
		ship40_50_min10_20_180_15_4E2.explode();
		ship100_100_min10_min20_135_20_6E4.getTimeToCollision(ship40_50_min10_20_180_15_4E2);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getTimeToCollision_WithArgs_DifferentWorldCase() throws Exception {
		ship40_50_min10_20_180_15_4E2.setWorld(world800_1280);
		ship60_30_15_20_113_16_52E6.setWorld(world1000_1600);
		ship40_50_min10_20_180_15_4E2.getTimeToCollision(ship60_30_15_20_113_16_52E6);
	}
	
	@Test
	public final void getTimeToCollision_NoArgs_CollisionCase() throws Exception {
		ship100_100_min10_min20_135_20_6E4.setWorld(world800_1280); //raakt beneden
		assert Util.fuzzyEquals(4,ship100_100_min10_min20_135_20_6E4.getTimeToCollision());
		ship150_70_15_30_90_10_8E14.setWorld(world800_1280); //raakt boven
		assert Util.fuzzyEquals(24, ship150_70_15_30_90_10_8E14.getTimeToCollision());
		ship200_125_min25_5_0_25_5E5.setWorld(world900_1440);	//raakt links
		assert Util.fuzzyEquals(7, ship200_125_min25_5_0_25_5E5.getTimeToCollision());
		ship600_400_20_10_180_20_7E2.setWorld(world1000_1600);	//raakt rechts
		assert Util.fuzzyEquals(49, ship600_400_20_10_180_20_7E2.getTimeToCollision());
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getTimeToCollision_NoArgs_TerminatedCase() throws Exception {
		ship150_70_15_30_90_10_8E14.setWorld(world800_1280);
		ship150_70_15_30_90_10_8E14.explode();
		ship150_70_15_30_90_10_8E14.getTimeToCollision();
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getTimeToCollision_NoArgs_NoWorldCase() throws Exception {
		ship0_34_0_min10_135_11_13E4.getTimeToCollision();
	}

	@Test
	public final void getCollisionPosition_WithArgs_CollisionCase() throws Exception {	
		double[] vector = ship20_0_0_10_0_13_98E3.getCollisionPosition(ship0_30_10_0_108_15_73E2);
		assert Util.fuzzyEquals(13.42699383, vector[0]);
		assert Util.fuzzyEquals(17.05861925, vector[1]);
	}
	
	@Test
	public final void getCollisionPosition_WithArgs_OverlapCase() throws Exception {			
		assertNull(ship0_30_10_0_108_15_73E2.getCollisionPosition(ship0_34_0_min10_135_11_13E4));	
		assertNull(ship0_30_0_min20_0_15_29E10.getCollisionPosition(ship0_34_0_min10_135_11_13E4));
		
	}
	
	@Test
	public final void getCollisionPosition_WithArgs_NoCollisionCase() throws Exception  {			
		assertNull(ship40_min10_10_0_0_12_1.getCollisionPosition(ship0_0_0_min10_45_15_20E6));
		assertNull(ship0_30_10_0_108_15_73E2.getCollisionPosition(ship40_min10_10_0_0_12_1));
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getCollisionPosition_WithArgs_InitialContactCase() throws Exception {			
		ship100_100_min10_min20_135_20_6E4.setWorld(world900_1440);
		ship70_100_min10_20_45_10_5E10.setWorld(world900_1440);
		assertNull(ship100_100_min10_min20_135_20_6E4.getCollisionPosition(ship70_100_min10_20_45_10_5E10));
	}
	
	@Test(expected = NullPointerException.class)
	public final void getCollisionPosition_WithArgs_NullPointerCase() throws Exception {			
		ship0_0_0_min10_45_15_20E6.getCollisionPosition(null);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getCollisionPosition_WithArgs_ThisTerminatedCase() throws Exception {
		ship100_100_min10_min20_135_20_6E4.setWorld(world800_1280);
		ship200_125_min25_5_0_25_5E5.setWorld(world800_1280);
		ship100_100_min10_min20_135_20_6E4.explode();
		ship100_100_min10_min20_135_20_6E4.getCollisionPosition(ship200_125_min25_5_0_25_5E5);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getCollisionPosition_WithArgs_OtherTerminatedCase() throws Exception {
		ship100_100_min10_min20_135_20_6E4.setWorld(world800_1280);
		ship200_125_min25_5_0_25_5E5.setWorld(world800_1280);
		ship200_125_min25_5_0_25_5E5.explode();
		ship100_100_min10_min20_135_20_6E4.getCollisionPosition(ship200_125_min25_5_0_25_5E5);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getCollisionPosition_WithArgs_DifferentWorldCase() throws Exception {
		ship40_50_min10_20_180_15_4E2.setWorld(world800_1280);
		ship60_30_15_20_113_16_52E6.setWorld(world1000_1600);
		ship40_50_min10_20_180_15_4E2.getCollisionPosition(ship60_30_15_20_113_16_52E6);
	}
	
	@Test
	public final void getCollisionPosition_NoArgs_CollisionCase() throws Exception {
		ship100_100_min10_min20_135_20_6E4.setWorld(world800_1280);	//raakt beneden
		assert Util.fuzzyEquals(60,ship100_100_min10_min20_135_20_6E4.getCollisionPosition()[0]);
		assert Util.fuzzyEquals(0,ship100_100_min10_min20_135_20_6E4.getCollisionPosition()[1]);
		ship150_70_15_30_90_10_8E14.setWorld(world800_1280);	//raakt boven
		assert Util.fuzzyEquals(510, ship150_70_15_30_90_10_8E14.getCollisionPosition()[0]);
		assert Util.fuzzyEquals(800, ship150_70_15_30_90_10_8E14.getCollisionPosition()[1]);
		ship200_125_min25_5_0_25_5E5.setWorld(world900_1440);	//raakt links
		assert Util.fuzzyEquals(0, ship200_125_min25_5_0_25_5E5.getCollisionPosition()[0]);
		assert Util.fuzzyEquals(160, ship200_125_min25_5_0_25_5E5.getCollisionPosition()[1]);
		ship600_400_20_10_180_20_7E2.setWorld(world1000_1600);	//raakt rechts
		assert Util.fuzzyEquals(1600, ship600_400_20_10_180_20_7E2.getCollisionPosition()[0]);
		assert Util.fuzzyEquals(890, ship600_400_20_10_180_20_7E2.getCollisionPosition()[1]);
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getCollisionPosition_NoArgs_TerminatedCase() throws Exception {
		ship60_30_15_20_113_16_52E6.setWorld(world1000_1600);
		ship60_30_15_20_113_16_52E6.explode();
		ship60_30_15_20_113_16_52E6.getCollisionPosition();
	}
	
	@Test(expected = IllegalStateException.class)
	public final void getCollisionPosition_NoArgs_NoWorldCase() throws Exception {
		ship600_400_20_10_180_20_7E2.getCollisionPosition();
	}
	
	@Test
	public final void bounce_NoArgs_ReverseXVelocityCase() throws Exception {
		ship200_125_min25_5_0_25_5E5.setWorld(world900_1440);
		ship200_125_min25_5_0_25_5E5.move(7);
		ship200_125_min25_5_0_25_5E5.bounce();
		assert Util.fuzzyEquals(25, ship200_125_min25_5_0_25_5E5.getXVelocity());
		assert Util.fuzzyEquals(5, ship200_125_min25_5_0_25_5E5.getYVelocity());
	}
	
	@Test
	public final void bounce_NoArgs_ReverseYVelocityCase() throws Exception {
		ship100_100_min10_min20_135_20_6E4.setWorld(world800_1280);
		ship100_100_min10_min20_135_20_6E4.move(4);
		ship100_100_min10_min20_135_20_6E4.bounce();
		assert Util.fuzzyEquals(-10,ship100_100_min10_min20_135_20_6E4.getXVelocity());
		assert Util.fuzzyEquals(20, ship100_100_min10_min20_135_20_6E4.getYVelocity());
	}
	
	@Test
	public final void bounce_NoArgs_ReverseBothCase() throws Exception {
		ship100_100_min10_min10_min135_10_7E13.setWorld(world900_1440);
		ship100_100_min10_min10_min135_10_7E13.move(9);
		ship100_100_min10_min10_min135_10_7E13.bounce();
		assert Util.fuzzyEquals(10, ship100_100_min10_min10_min135_10_7E13.getXVelocity());
		assert Util.fuzzyEquals(10, ship100_100_min10_min10_min135_10_7E13.getYVelocity());
	}
	
	@Test(expected = IllegalStateException.class)
	public final void bounce_NoArgs_NoWorldCase() throws Exception {
		ship0_0_min15_0_157_50_61.bounce();
	}
	
	@Test(expected = IllegalStateException.class)
	public final void bounce_NoArgs_TerminatedCase() throws Exception {
		ship200_125_min25_5_0_25_5E5.setWorld(world1000_1600);
		ship200_125_min25_5_0_25_5E5.move(4);
		ship200_125_min25_5_0_25_5E5.explode();
		ship200_125_min25_5_0_25_5E5.bounce();
	}
	
	@Test(expected = IllegalStateException.class)
	public final void bounce_NoArgs_IllegalPositionCase() throws Exception {
		ship200_125_min25_5_0_25_5E5.setWorld(world800_1280);
		ship200_125_min25_5_0_25_5E5.bounce();
	}
	
	@Test 
	public final void explode_SingleCase() {
		Ship ship = new Ship();
		ship.explode();
		assertTrue(ship.isTerminated());
	}

	@Test
	public final void evolve_LegalCase() {
		ship150_70_15_30_90_10_8E14.setWorld(world1000_1600);
		ship150_70_15_30_90_10_8E14.setThrusterOn(true);
		ship150_70_15_30_90_10_8E14.evolve(5);
		assert Util.fuzzyEquals(225, ship150_70_15_30_90_10_8E14.getXPosition());
		assert Util.fuzzyEquals(220, ship150_70_15_30_90_10_8E14.getYPosition());
		assert Util.fuzzyEquals(15, ship150_70_15_30_90_10_8E14.getXVelocity());
		assert Util.fuzzyEquals(6.905E3, ship150_70_15_30_90_10_8E14.getYVelocity());
	}
	
	@Test(expected = IllegalStateException.class)
	public final void evolve_NoWorldCase() {
		ship0_0_min15_0_157_50_61.evolve(10);
	}
	
	@Test
	public final void evolve_ZeroDurationCase() {
		ship200_125_min25_5_0_25_5E5.setWorld(world1000_1600);
		ship200_125_min25_5_0_25_5E5.evolve(0);
		assert Util.fuzzyEquals(200, ship200_125_min25_5_0_25_5E5.getXPosition());
		assert Util.fuzzyEquals(125, ship200_125_min25_5_0_25_5E5.getYPosition());
		assert Util.fuzzyEquals(-25, ship200_125_min25_5_0_25_5E5.getXVelocity());
		assert Util.fuzzyEquals(5, ship200_125_min25_5_0_25_5E5.getYVelocity());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void evolve_NegativeDurationCase() {
		ship200_125_min25_5_0_25_5E5.setWorld(world1000_1600);
		ship200_125_min25_5_0_25_5E5.evolve(-3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public final void evolve_NaNDurationCase() {
		ship200_125_min25_5_0_25_5E5.setWorld(world1000_1600);
		ship200_125_min25_5_0_25_5E5.evolve(Double.NaN);
	}	
}
