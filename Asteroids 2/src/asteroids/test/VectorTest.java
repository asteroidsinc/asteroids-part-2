/**
 * 
 */
package asteroids.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import asteroids.util.*;

/**
 * A class for testing the functionalities of the class Vector.
 * 
 * @author 	Romain Carlier (2Bcwswtk) & Beno�t de Braband�re (2Beltcws1)
 * @version 2.3.9
 * @link	https://bitbucket.org/asteroidsinc/asteroids-part-2
 */
public class VectorTest {

	private Vector vector0_0;
	private Vector vector5_1;
	private Vector vector2_7;
	private Vector vectormin4_3;
	private Vector vector8_min6;
	private Vector vectormin2_min5;
	private Vector vector1_inf;
	private Vector vectorinf_min6;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		vector0_0 = new Vector(0,0);
		vector5_1 = new Vector(5,1);
		vector2_7 = new Vector(2,7);
		vectormin4_3 = new Vector(-4,3);
		vector8_min6 = new Vector(8,-6);
		vectormin2_min5 = new Vector(-2,-5);
		vector1_inf = new Vector(1,Double.POSITIVE_INFINITY);
		vectorinf_min6 = new Vector(Double.NEGATIVE_INFINITY, -6);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void arrayVector_singleCase() {
		assert Util.fuzzyEquals(5, Vector.arrayVector(vector5_1)[0]);
		assert Util.fuzzyEquals(1, Vector.arrayVector(vector5_1)[1]);
	}
	
	@Test
	public final void opposite_singleCase() {
		assert Util.fuzzyEquals(-5, Vector.opposite(vector5_1).X_COORD);
		assert Util.fuzzyEquals(-1, Vector.opposite(vector5_1).Y_COORD);
		assert Util.fuzzyEquals(0, Vector.opposite(vector0_0).X_COORD);
		assert Util.fuzzyEquals(0, Vector.opposite(vector0_0).Y_COORD);
		assert Util.fuzzyEquals(Double.POSITIVE_INFINITY, Vector.opposite(vectorinf_min6).X_COORD);
		assert Util.fuzzyEquals(6, Vector.opposite(vectorinf_min6).Y_COORD);
	}
	
	@Test
	public final void addition_singleCase() {
		assert Util.fuzzyEquals(7, Vector.addition(vector5_1,vector2_7).X_COORD);
		assert Util.fuzzyEquals(8, Vector.addition(vector5_1, vector2_7).Y_COORD);
		//infinity
	}
	
	@Test
	public final void substraction_singleCase() {
		assert Util.fuzzyEquals(3, Vector.substraction(vector5_1,vector2_7).X_COORD);
		assert Util.fuzzyEquals(-6, Vector.substraction(vector5_1, vector2_7).Y_COORD);
		//infinity
	}
	
	@Test
	public final void multiple_singleCase() {
		assert Util.fuzzyEquals(20, Vector.multiple(vector5_1,4).X_COORD);
		assert Util.fuzzyEquals(7, Vector.multiple(vector5_1, 7).Y_COORD);
		//infinity
	}
	
	@Test
	public final void norm_singleCase() {
		assert Util.fuzzyEquals(Math.sqrt(26), Vector.norm(vector5_1));
	}
	
	@Test
	public final void dotProduct_singleCase() {
		assert Util.fuzzyEquals(17, Vector.dotProduct(vector5_1,vector2_7));
		assert Util.fuzzyEquals(14, Vector.dotProduct(vector8_min6, vectormin2_min5));
	}
	
	@Test
	public final void distance_singleCase() {
		assert Util.fuzzyEquals(Math.sqrt(45), Vector.distance(vector5_1,vector2_7));
		assert Util.fuzzyEquals(Math.sqrt(225), Vector.distance(vector8_min6, vectormin4_3));
	}
	
//	@Test
//	public final void Double_GreatValueCase() {
//		assert Util.fuzzyGreaterThan(Double.POSITIVE_INFINITY, Double.MAX_VALUE);
//		assert Util.fuzzyGreaterThan(Double.MAX_VALUE+1, 0);
//		assert Util.fuzzyEquals(1-Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY);
//		assert Util.fuzzyEquals(-1.0/0.0, Double.NEGATIVE_INFINITY);
//	}
	
	@Test
	public final void fuzzyTest() {
		assert Util.fuzzyLessThanOrEqualTo(0.0001,0.0);
	}
	
	@Test
	public final void fuzzyEquals_NaNCase() {
		assertFalse(Util.fuzzyEquals(Double.NaN, 1));
		assertFalse(Util.fuzzyEquals(0.0, Double.NaN));
	}
}
